import Plotly from 'plotly.js-dist';
const d3 = require("d3");

const chromosomes = [
  "chr1",
  "chr2",
  "chr3",
  "chr4",
  "chr5",
  "chr6",
  "chr7",
  "chr8",
  "chr9",
  "chr10",
  "chr11",
  "chr12",
  "chr13",
  "chr14",
  "chr15",
  "chr16",
  "chr17",
  "chr18",
  "chr19",
  "chr20",
  "chr21",
  "chr22"
];

const color = ['orangered', '#4287f5'];

let trace1 = {
  x: [],
  y: [],
  type: 'scatter',
};

const tableLines = [];

export default {
  display: 'Scatter',
  data: {
    data: [trace1],
    attr: { displayModeBar: false },
    layout: {
      plot_bgcolor: '#d3d3d3',
      paper_bgcolor: '#d3d3d3',
    },
    datarows: []
  },
  updateTrace() {
    trace1 = {
      x: [1, 2, 3, 4],
      y: [10, 15, 13, 17],
      type: 'scatter',
    };

    this.data.data = [trace1];
  },
  makePlot(parent) {
    const plotHeatMapSize = 1100;
    const why = this;
    const idProject = parent.project.id;

    d3.csv(process.env.API_URL + "/projects/" + idProject + "/genome")
      .then((rows) => {
        why.data.datarows = rows;
        const dataplot = chromosomes.map((chr) => {
          const rowsFiltered = rows.filter((row) => {
            return (row.CHR === chr);
          });
          const xvar = unpack(rowsFiltered, 'Region');
          const yvar = unpackLog(rowsFiltered, 'JOSTmin');
          return {
            name: chr,
            x: xvar,
            y: yvar,
            text: unpackText(rowsFiltered),
            hovertemplate: '%{text}',
            mode: 'markers',
            showlegend: false,
            type: 'scatter',
            marker: {
              color: 'red'
            }
          }
        });

        why.data.data = dataplot;

        const layout = {
          title: "Joint test association results by regions",
          width: plotHeatMapSize,
          hovermode: 'closest',
          showLink: false,
          modeBarButtonsToRemove: [],
          font: {
            size: 8
          },
          margin: {
            l: 80,
            r: 50,
            b: 60,
            t: 30,
            pad: 4
          },
          xaxis: {
            showgrid: true,
            showticklabels: true,
            tickmode: "array",
            tickvals: [
              'Region67',
              'Region206',
              'Region339',
              'Region461',
              'Region577',
              'Region688',
              'Region793',
              'Region890',
              'Region974',
              'Region1053',
              'Region1138',
              'Region1221',
              'Region1293',
              'Region1352',
              'Region1405',
              'Region1457',
              'Region1507',
              'Region1555',
              'Region1598',
              'Region1637',
              'Region1668',
              'Region1692'
            ],
            ticktext: [
              '1',
              '2',
              '3',
              '4',
              '5',
              '6',
              '7',
              '8',
              '9',
              '10',
              '11',
              '12',
              '13',
              '14',
              '15',
              '16',
              '17',
              '18',
              '19',
              '20',
              '21',
              '22'
            ],
            tickfont: {
              family: 'Arial, sans-serif',
              size: 12,
              color: 'black',
            },
            title: "chromosome",
            side: 'bottom',
            type: 'category',
            range: [0, '1704'],
          },
          yaxis: {
            range: [0, '2.3'],
            title: "-log(Pvalue)",
            type: 'log',
            tickfont: {
              family: 'Arial, sans-serif',
              size: 10,
              color: 'black'
            }
          },
        };

        why.data.layout = layout

        let k = 1;
        let i;
        let j;
        const markerOffset = 0.04;

        for (i = 0; i < dataplot.length; i++) {

          k = 1 - k;
          dataplot[i].marker.color = color[k];

          for (j = 0; j < dataplot[i].x.length; j++) {
            const dict = {
              "type": 'line',
              "xref": 'x',
              "yref": 'y',
              "x0": dataplot[i].x[j],
              "y0": 0.01,
              "x1": dataplot[i].x[j],
              "y1": offsetSignal(dataplot[i].y[j], markerOffset),
              "line": {
                color: dataplot[i].marker.color,
                width: 0.3
              }
            };

            tableLines.push(dict);
          }
        }

        const update = {
          shapes: tableLines
        };

        const suggestiveline = -Math.log10(1e-5);
        const suggestivelineColor = "orange";

        const genomewideline = -Math.log10(5e-8);
        const genomewidelineColor = "green";

        const datmp = dataplot[0];
        const tbx = datmp.x;
        const d = tbx[0];

        const datmp2 = dataplot[dataplot.length - 1];
        const tbx2 = datmp2.x
        const e = tbx2[tbx2.length - 1];

        const trace1 = {
          x: [d, e],
          y: [suggestiveline, suggestiveline],
          mode: 'lines+markers+text',
          name: '1e-5',
          textposition: 'top',
          type: 'scatter',
          showlegend: true,
          marker: {
            color: suggestivelineColor
          }
        };

        const trace2 = {
          x: [d, e],
          y: [genomewideline, genomewideline],
          mode: 'lines+markers+text',
          name: '5e-8',
          textposition: 'top',
          type: 'scatter',
          showlegend: true,
          marker: {
            color: genomewidelineColor
          }
        };

        why.data.layout.shapes = tableLines;
        why.data.data.push(trace1);
        why.data.data.push(trace2);

        Plotly.newPlot('divWholeStatPlot', why.data.data, why.data.layout, { displayModeBar: false });
        parent.$refs.divWholeStatPlot.on('plotly_click', function (data) {
          parent.isready2 = true;
          why.reactOnClick(data);
          Plotly.update('divWholeStatPlot', why.data.layout);

          const selectedChr = data.points.map(function (d) {
            return d.data.name;

          });

          const selectedRegion = data.points[0].x;
          parent.heatmap.makePlot(parent, selectedChr, selectedRegion);

        });

      });
  },
  reactOnClick(data) {
    let updaterange = data.points[0].x.split("Region");
    const tmp = Number(updaterange[1]) + 10;
    updaterange = "Region" + tmp;
    const selectRectangle = {
      "type": 'rect',
      // x-reference is assigned to the x-values
      "xref": 'x',
      // y-reference is assigned to the plot paper [0,1]
      "yref": 'paper',
      "x0": data.points[0].x,
      "y0": 0,
      "x1": updaterange,
      "y1": 1,
      "fillcolor": '#d3d3d3',
      "opacity": 0.6,
      "line": {
        width: 0
      }
    };

    if (tableLines[tableLines.length - 1].type === 'rect') {
      tableLines.pop();
    }

    tableLines.push(selectRectangle);
    this.data.layout.shapes = tableLines;
  }
};

// ---------------------------------------------------------------------------------------------------------------------------

function offsetSignal(signal, markerOffset) {
  let result = 0;
  if (Math.abs(signal) > markerOffset) {
    if (signal > 0) {
      result = signal - markerOffset;
    }
    else {
      result = signal + markerOffset;
    }
  }
  return result;
}

// ---------------------------------------------------------------------------------------------------------------------------

function unpack(rows, key) {
  return rows.map((row) => { return row[key]; });
}

// ---------------------------------------------------------------------------------------------------------------------------

function unpackLog(rows, key) {
  return rows.map((row) => { return -Math.log10(row[key]); });
}

// ---------------------------------------------------------------------------------------------------------------------------

function unpackText(rows) {
  return rows.map(function (row) {
    const rsid = row.snp_ids;
    const pos = row.MiddlePosition / 1000000;
    const chr = row.CHR;
    const pval = row.JOSTmin;
    const text = 'rsid: ' + rsid + '<br>' + 'pos: ' + chr + ':' + pos.toFixed(2) + '<br>-log(Pvalue): ' + pval + '<extra></extra>';
    return text;
  });
}
