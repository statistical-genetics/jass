import Plotly from 'plotly.js-dist';
const d3 = require("d3");

const color = ['red', 'blue'];

const trace1 = {
  x: [],
  y: [],
  type: 'scatter',
};
const plotHeatMapSize = 1100;

export default {
  display: 'Scatter',
  data: {
    data: [trace1],
    attr: { displayModeBar: false },
    layout: {
      plot_bgcolor: '#d3d3d3',
      paper_bgcolor: '#d3d3d3',
    },
  },
  makePlot(parent, selectedChr, selectedRegion) {
    const why = this;
    const numberColor = (selectedChr + 1) % 2;
    const idProject = parent.project.id;

    d3.csv(process.env.API_URL + "/projects/" + idProject + "/manhattan/" + selectedChr + "/" + selectedRegion)
      .then((allRows) => {
        const regions = [];

        for (let i = 0; i < 2000; i++) {
          regions[i] = "Region" + i;
        }

        const data = regions.map((region) => {
          const rowsFiltered = allRows.filter((row) => {
            return (row.Region === region);
          });

          return {
            name: region,
            x: unpack(rowsFiltered, 'snp_ids'),
            y: unpackLog(rowsFiltered, 'JASS_PVAL'),
            text: unpack(rowsFiltered, 'Region'),
            mode: 'markers',
            type: 'scatter',
            marker: {
              color: color[numberColor]
            }
          }
        });

        const titleplot = "Joint test association results for locus " + selectedRegion + " on " + selectedChr;

        const layout = {
          title: titleplot,
          width: plotHeatMapSize,
          hovermode: 'closest',
          font: {
            size: 8
          },
          margin: {
            l: 90,
            r: 90,
            b: 10,
            t: 100,
            pad: 4
          },
          xaxis: {
            showticklabels: false,
            ticks: '',
            side: 'top',
            type: 'category',
            range: [-0.5, allRows.length]
          },
          yaxis: {
            title: "-log(Pvalue)",
            ticks: '',
            ticksuffix: ' ',
            fixedrange: true,
          },
        };

        why.data.data = data;
        why.data.layout = layout;

        Plotly.newPlot('divLocalStatPlot', why.data.data, why.data.layout, { displayModeBar: false });

        parent.$refs.divLocalStatPlot.on('plotly_relayout',
          function (eventdata) {
            const update = {
              'xaxis.range': [eventdata['xaxis.range[0]'],
              eventdata['xaxis.range[1]']]
            }
            Plotly.relayout('divHeatPlot', update);
          });
      });
  }
}

function unpack(rows, key) {
  return rows.map((row) => { return row[key]; });
}

// ---------------------------------------------------------------------------------------------------------------------------

function unpackLog(rows, key) {
  return rows.map((row) => { return -Math.log10(row[key]); });
}

// ---------------------------------------------------------------------------------------------------------------------------

function unpackText(rows) {
  return rows.map((row) => {
    const rsid = row.snp_ids;
    const pos = row.MiddlePosition / 1000000;
    const chr = row.CHR;
    const pval = row.JOSTmin;
    const text = 'rsid: ' + rsid + '<br>' + 'pos: ' + chr + ':' + pos.toFixed(2) + '<br>-log(Pvalue): ' + pval + '<extra></extra>';
    return text;
  });
}
