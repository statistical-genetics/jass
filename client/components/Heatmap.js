import Plotly from 'plotly.js-dist';
const d3 = require("d3");

const trace1 = {
  x: [],
  y: [],
  type: 'scatter',
};
const plotHeatMapSize = 1100;

export default {
  display: 'Scatter',
  data: {
    data: [trace1],
    attr: { displayModeBar: false },
    layout: {
      plot_bgcolor: '#d3d3d3',
      paper_bgcolor: '#d3d3d3',
    },
  },
  makePlot(parent, selectedChr, selectedRegion) {
    const layout = {
      title: "Z score Heatmap",
      autosize: false,
      width: plotHeatMapSize,
      font: {
        size: 8
      },
      margin: {
        l: 90,
        r: 50,
        b: 60,
        t: 30,
        pad: 4
      },
      xaxis: {
        showticklabels: true,
        ticks: '',
        side: 'bottom',
        fixedrange: true,
        type: 'category'
      },
      yaxis: {
        ticks: '',
        fixedrange: true,
        ticksuffix: ' '
      },
      showlegend: false
    };

    const why = this;
    const idProject = parent.project.id;

    d3.csv(process.env.API_URL + "/projects/" + idProject + "/heatmap/" + selectedChr + "/" + selectedRegion).then(
      function (allRows) {
        const cols = [];
        const lines = [];

        for (const valeur in allRows[0]) {
          if ((valeur !== "") && (valeur !== 'ID')) {
            cols.push(valeur);
          }
        }

        const arr = [];
        let row = allRows[0];

        for (let i = 0; i < allRows.length; i++) {
          row = allRows[i];
          arr.push([]);
          lines[i] = row.ID;
          arr[i].push(new Array(cols));
          for (let j = 0; j < cols.length; j++) {
            arr[i][j] = row[cols[j]];
          }
        }

        const data = [
          {
            z: arr,
            x: cols,
            y: lines,
            type: 'heatmap',
            colorscale: [
              ['-1', '#4A6FE3'],
              ['-0.9', '#5F7BE1'],
              ['-0.8', '#7086E1'],
              ['-0.7', '#8091E1'],
              ['-0.6', '#8F9DE1'],
              ['-0.5', '#9DA8E2'],
              ['-0.4', '#ABB4E2'],
              ['-0.3', '#B9BFE3'],
              ['-0.2', '#C7CBE3'],
              ['-0.1', '#D5D7E3'],
              ['0', '#E2E2E2'],
              ['0.1', '#E4D3D6'],
              ['0.2', '#E6C4C9'],
              ['0.3', '#E6B4BD'],
              ['0.4', '#E5A5B1'],
              ['0.5', '#E495A5'],
              ['0.6', '#E28699'],
              ['0.7', '#DF758D'],
              ['0.8', '#DB6581'],
              ['0.9', '#D75376'],
              ['1', '#D33F6A']
            ],
            transforms: [{
              type: 'filter',
              target: 'y',
              operation: '!=',
              value: 'PVALmin'
            }]
          }
        ];

        why.data.data = data;
        why.data.layout = layout;

        Plotly.newPlot('divHeatPlot', why.data.data, why.data.layout, { displayModeBar: false });
        parent.submanhattan.makePlot(parent, selectedChr, selectedRegion);
      }
    );
  }
}
