import Vue from 'vue'
import DOMPurify from 'dompurify'

/**
 * Use DOMpurify to sanitize html or markdown strings.
 * This directive should be used instead of the native v-html.
 */
Vue.directive('sanitized-html', {
  bind: (el, binding) => {
    el.innerHTML = DOMPurify.sanitize(binding.value)
  },
})
