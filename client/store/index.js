// Main store for getting and sharing initTables data
// across components.
// https://v2.nuxt.com/docs/directory-structure/store

const _ = require('lodash');

export const state = () => ({
  initTables: {}
})

export const getters = {
  getTotalTables(state) {
    return Object.keys(state.initTables).length
  },
  getTotalPhenotypes(state) {
    return Object.values(state.initTables).reduce((total, table) => {
      if (typeof (table.nb_phenotypes) === 'number') {
        return total + table.nb_phenotypes
      } else {
        return total
      }
    }, 0)
  },
  getTotalSNP(state) {
    return Object.values(state.initTables).reduce((total, table) => {
      if (typeof (table.nb_snps) === 'number') {
        return total + table.nb_snps
      } else {
        return total
      }
    }, 0)
  },
}

export const mutations = {
  setInitTables(state, initTables) {
    state.initTables = initTables
  }
}

export const actions = {
  async fetchTables({ commit }) {
    const initTables = []

    return await this.$axios.$get('/tables')
      .then((initTableslist) => {
        const initMetaRequests = initTableslist.map(initTableName => {
          return this.$axios
            .$post('/initmeta', { initTableName })
            .then(initMeta => {
              initMeta.tableName = initTableName
              initTables.push(initMeta)
            })
        })

        return Promise.all(initMetaRequests)
      })
      .then(() => {
        commit('setInitTables', _.orderBy(initTables, 'name'))
      })
  }
}
