# Configure you local machine 

```shell
K8S_USER=${USER} # MUST BE YOU SHORT LOGIN
kubectl config set-context jass-dev --cluster=k8sdev-01 --user=${K8S_USER}@k8sdev-01 --namespace=jass-dev
kubectl config set-context jass-prod --cluster=k8sprod-02 --user=${K8S_USER}@k8sprod-02 --namespace=jass-prod
```

# Starting a new namespace

As the data ar not that small, Bryan decided to put the content of /data in a shared pvc, and that all JASS instances in
a namespace use this shared pvc to get the data (initTable.hdf5, ...). When starting a new namespace, prior to any 
JASS deployment, the data must already me there. You have to create the pvc, and then copy the data
(cf. Updating initTable.hdf5).

### Create/update the pvc and the sidekick

```shell
kubectl config use-context jass-dev
kubectl apply -f chart/pvc-shared-data.yaml
```

# Updating /data, including initTable.hdf5

### Update the pvc, start the sidekick

```shell
kubectl config use-context jass-dev
kubectl apply -f chart/sidekick.yaml -f chart/pvc-shared-data.yaml
```

### Copy the data

```shell
kubectl config use-context jass-dev
kubectl cp ./data sidekick:/data
# all data are not where they should be yet, you have to put them one level higher
kubectl exec sidekick -- ls -lah /data
kubectl exec sidekick -- sh -c "mv /data/data/* /data/"
kubectl exec sidekick -- rmdir /data/data
kubectl exec sidekick -- ls -lah /data
```

### Alternative: Copy only initTable.hdf5

```shell
kubectl config use-context jass-dev
kubectl cp ./data/initTable.hdf5 sidekick:/data/initTable.hdf5
kubectl exec sidekick -- ls -lah /data
```

### shutdown the sidekick
```shell
kubectl config use-context jass-dev
kubectl delete -f chart/sidekick.yaml
```