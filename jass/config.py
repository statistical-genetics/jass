"""
This module contains the code to access the configuration of JASS

The different variables have reasonable defaults, that can be overridden
with environment variables:

.. code-block:: shell

   # set specific host and ports for the JASS server to listen to
   export JASS_HOST='0.0.0.0'
   export JASS_PORT='8080'
   # set specific directory to store JASS data
   export JASS_DATA_DIR='/var/jassdata'

"""

import os

config = {
    "HOST": "0.0.0.0",
    "PORT": "8080",
    "DATA_DIR": os.path.normpath(
        os.path.join(os.path.dirname(os.path.abspath(__file__)), "..", "data")
    ),
    "PROJECTS_DIR": os.path.normpath(
        os.path.join(os.path.dirname(os.path.abspath(__file__)), "..", "projects")
    ),
    "INITTABLE_CHUNKSIZE": 100000,
    "MIN_SIZE_TO_KEEP_IN_PROJECTS_DIR_IN_MB": int(os.environ.get('MIN_SIZE_TO_KEEP_IN_PROJECTS_DIR_IN_MB', '0')),
}
if "JASS_HOST" in os.environ:
    config["HOST"] = os.environ["JASS_HOST"]
if "JASS_PORT" in os.environ:
    config["PORT"] = os.environ["JASS_PORT"]
if "JASS_DATA_DIR" in os.environ:
    config["DATA_DIR"] = os.environ["JASS_DATA_DIR"]
if "JASS_PROJECTS_DIR" in os.environ:
    config["PROJECTS_DIR"] = os.environ["JASS_PROJECTS_DIR"]
config["CELERY_RESULTS_BACKEND_DIR"] = os.environ.get(
    "CELERY_RESULTS_BACKEND_DIR",
    os.path.join(config["PROJECTS_DIR"], "celery_results"),
)
os.makedirs(config["CELERY_RESULTS_BACKEND_DIR"], exist_ok=True)