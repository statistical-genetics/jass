import os
import sys
import traceback
from typing import List, Dict

from celery import Celery, group, chain
from fastapi import HTTPException
from flask import Flask

import jass.models.project
from jass.models.project import GlobalProject, Project, ensure_space_in_project_dir

from jass.models.phenotype import Phenotype, get_available_phenotypes
from jass.config import config


def make_celery(app):
    celery = Celery()
    if "CELERY_CONFIG_MODULE" in os.environ:
        celery.config_from_envvar("CELERY_CONFIG_MODULE")
    else:
        celery.config_from_object("jass.celeryconfig")
    celery.conf.update(app.config)
    TaskBase = celery.Task

    class ContextTask(TaskBase):
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)

    celery.Task = ContextTask
    return celery


flask_app = Flask(__name__)
celery = make_celery(flask_app)


@celery.task
def ensure_space_in_project_dir_task(project_id):
    ensure_space_in_project_dir(except_project_id=project_id)


@celery.task
def create_project_worktable(project_id):
    project = GlobalProject.load(project_id)
    return project.create_worktable_file()


@celery.task
def create_project_global_plot(project_id):
    project = GlobalProject.load(project_id)
    return project.create_global_manhattan_plot()


@celery.task
def create_project_quadrant_plot(project_id):
    project = GlobalProject.load(project_id)
    return project.create_quadrant_plot()


@celery.task
def create_project_local_plot(project_id):
    project = GlobalProject.load(project_id)
    return project.create_local_plot()


@celery.task
def create_project_qq_plot(project_id):
    project = GlobalProject.load(project_id)
    return project.create_qq_plot()


@celery.task
def create_project_csv_file(project_id):
    project = GlobalProject.load(project_id)
    return project.create_csv_file()


@celery.task
def create_project_metadata_file(project_id):
    project = GlobalProject.load(project_id)
    return project.create_project_metadata_file()


@celery.task
def dummy_task():
    print("This task to nothing, but help the chain to have a valid status")


def get_queue_status():
    inspect = celery.control.inspect()
    workers = set()
    ret = {'active': 0, 'reserved': 0}
    for k in ['active', 'reserved']:
        try:
            for worker, queue in getattr(inspect, k)().items():
                ret[k] += len(queue)
                workers.add(worker)
        except (
                AttributeError,  # when no worker
                ConnectionResetError,  # when rabbitMQ went down
                TimeoutError,  # when rabbitMQ is not reachable
        ):
            pass
    ret['worker'] = len(workers)
    return ret


def run_project_analysis_if_needed(project):
    if project.has_computation_going_on():
        return
    tasks = []
    post_worktable_tasks = []

    if not os.path.exists(project.get_worktable_path()):
        tasks.append(create_project_worktable.si(project.id))

    if project.get_metadata_file_path() and not os.path.exists(project.get_metadata_file_path()):
        tasks.append(create_project_metadata_file.si(project.id))

    if project.get_global_manhattan_plot_path() and not os.path.exists(project.get_global_manhattan_plot_path()):
        post_worktable_tasks.append(create_project_global_plot.si(project.id))

    if project.get_quadrant_plot_path() and not os.path.exists(project.get_quadrant_plot_path()):
        post_worktable_tasks.append(create_project_quadrant_plot.si(project.id))

    if project.get_qq_plot_path() and not os.path.exists(project.get_qq_plot_path()):
        post_worktable_tasks.append(create_project_qq_plot.si(project.id))

    if project.get_csv_path() and not os.path.exists(project.get_csv_path()):
        post_worktable_tasks.append(create_project_csv_file.si(project.id))

    if len(tasks) + len(post_worktable_tasks) == 0:
        return
    main_wf = chain(
        # first we check again that there is enough disk space
        ensure_space_in_project_dir_task.si(project.id),
        # we run the main task(s)
        *tasks,
        # we then compute the missing charts
        group(post_worktable_tasks),
        # it is mandatory to add a task that do nothing if group is used in order to have a success/failure status
        dummy_task.si(),
    )
    task = main_wf.delay()
    project.celery_id = task.id
    # Artificially create a result for the chain/chord so its state will survive reboot of rabbitmq.
    # Otherwise, the status would be PENDING which we consider, even if no task are pending.
    task.backend.store_result(task.id, None, "SENT")
    project.save()


def create_project(
    phenotype_ids: List[str],
    chromosome: str = None,
    start: str = None,
    end: str = None,
    init_table_name: str = None,
):
    init_table_path = os.path.join(config["DATA_DIR"], init_table_name)
    available_phenotypes=get_available_phenotypes(init_table_path)
    unavailable_requested_ids = set(phenotype_ids).difference(
        set(phenotype.id for phenotype in available_phenotypes)
    )
    if len(unavailable_requested_ids) > 0:
        raise HTTPException(status_code=404, detail=f"Phenotype IDs not found: {','.join(unavailable_requested_ids)}")
    phenotypes = [
        phenotype for phenotype in available_phenotypes if phenotype.id in phenotype_ids
    ]
    project = GlobalProject(phenotypes=phenotypes, init_table_path=init_table_path)

    project.create(fail_if_exists=False)
    ensure_space_in_project_dir(except_project_id=project.id)

    # if project.get_type_of_analysis() == Project.LOCAL_ANALYSIS:
    #     # Local Analysis
    #     global_plot_path = None
    #     quadrant_plot_path = None
    #     zoom_plot_path = project.get_zoom_plot_path()
    #     delayed_gen_csv_file = False
    # else:
    #     # genome wide Analysis
    #     global_plot_path = project.get_global_manhattan_plot_path()
    #     quadrant_plot_path = project.get_quadrant_plot_path()
    #     zoom_plot_path = None
    #     delayed_gen_csv_file = True

    return project
