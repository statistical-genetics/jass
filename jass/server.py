#!/usr/bin/env python3
import logging
import os
import shutil
import traceback
from json import JSONDecodeError
from pathlib import Path
from typing import List

from starlette.responses import RedirectResponse, JSONResponse
from tables import HDF5ExtError

from jass import util
from jass.config import config
from jass.models.phenotype import Phenotype, get_available_phenotypes, PhenotypeIdList, InitTableNameModel
from jass.models.inittable import get_inittable_meta, valideInitTableNamePattern
from jass.models.project import GlobalProject, load_project as project__load_project
from jass.tasks import create_project, run_project_analysis_if_needed, get_queue_status

from fastapi import FastAPI, HTTPException
from fastapi.responses import Response, FileResponse
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_methods=["*"],
    allow_headers=["*"],
)

if os.path.exists('client/dist'):
    app.mount("/webui", util.SinglePageApplication(directory=Path('client/dist')), name="SPA")
else:
    logging.info("/webui cannot be served as client/dist is missing, please build the client.")


def load_project(project_id, *args, **kargs):
    try:
        return project__load_project(project_id=project_id, *args, **kargs)
    except FileNotFoundError:
        raise HTTPException(status_code=404, detail="Project not found")
    except JSONDecodeError as e:
        print(f"Project {project_id} has a corrupted meta.json ({str(e)}), removing it.")
        shutil.rmtree(GlobalProject.get_folder_path_from_id(project_id))
        raise HTTPException(status_code=404, detail="Project invalid, and removed")


@app.get("/")
async def read_index():
    return RedirectResponse(url="/webui/")


@app.get("/api/tables", response_model=List[str])
def inittable_list():
    """List initTables"""
    for filename in os.listdir(config["DATA_DIR"]):
        if (
            filename.endswith(".hdf5")
            and "worktable" not in filename.lower()
            and valideInitTableNamePattern.match(filename)
        ):
            yield filename


@app.post("/api/phenotypes", response_model=List[Phenotype])
def phenotypes_list(init_table_name: InitTableNameModel):
    """List phenotypes"""
    try:
        return get_available_phenotypes(init_table_name.get_init_table_path())
    except FileNotFoundError as e:  # initTable does not exists
        raise HTTPException(status_code=404, detail=str(e))

@app.post("/api/initmeta")
def inittable_meta(init_table_name: InitTableNameModel):
    """Number of phenotype and SNPs"""
    return get_inittable_meta(init_table_name.get_init_table_path())


@app.post("/api/projects", response_model=GlobalProject)
def project_create(phenotype_id_list: PhenotypeIdList):
    return create_project(
        phenotype_id_list.phenotypeID,
        init_table_name=phenotype_id_list.initTableName,
    )


@app.get("/api/projects/{project_id}", response_model=GlobalProject)
def project_detail(project_id: str):
    project = load_project(project_id=project_id)
    run_project_analysis_if_needed(project)
    return project


@app.get("/api/projects/{project_id}/progress", response_model = int )
def project_detail(project_id: str):
    return load_project(project_id=project_id).progress


@app.get("/api/projects/{project_id}/unload", response_model=bool)
def project_detail(project_id: str):
    return load_project(project_id=project_id, flag_as_visited=False).delete_large_files()


@app.get("/api/projects/{project_id}/summary")
def project_detail(project_id: str):
    return load_project(project_id=project_id).get_project_summary_statistics()


@app.get("/api/projects/{project_id}/gencov")
def project_gencov_view(project_id: str):
    try:
        return util.JSONNumpyResponse(load_project(project_id=project_id).get_project_gencov())
    except ValueError as e:  # NaN in values
        raise HTTPException(status_code=500, detail=str(e))
    except HDF5ExtError as e:  # file corrupted
        raise HTTPException(status_code=500, detail=str(e))
    except Exception as e:  # anything else
        traceback.print_exc()
        raise HTTPException(status_code=500, detail=str(e))


@app.get("/api/projects/{project_id}/metadata")
def project_metadata(project_id: str):
    return FileResponse(load_project(project_id=project_id).get_metadata_file_path())


@app.get("/api/projects/{project_id}/globalmanhattan")
def project_get_manhattan(project_id: str):
    return FileResponse(
        load_project(project_id=project_id).get_global_manhattan_plot_path(),
        media_type="image/png",
    )


@app.get("/api/projects/{project_id}/quadrant")
def project_get_quadrant(project_id: str):
    return FileResponse(
        load_project(project_id=project_id).get_quadrant_plot_path(),
        media_type="image/png",
    )


@app.get("/api/projects/{project_id}/qqplot")
def project_get_qq_plot(project_id: str):
    return FileResponse(
        load_project(project_id=project_id).get_qq_plot_path(),
        media_type="image/png",
    )


@app.get("/api/projects/{project_id}/genome_full")
def get_full_sumstat(project_id: str):
    project = load_project(project_id=project_id)
    if not os.path.exists(project.get_csv_path()):
        print("CREATING CSV FILE")
        project.create_csv_file()
        print("CREATED CSV FILE")
    return FileResponse(
        project.get_csv_path(),
        filename=f"genome_full_{project_id}.csv",
        media_type="text/csv",
    )


@app.get("/api/projects/{project_id}/genome")
def get_data_manhattan(project_id: str):
    return Response(
        content=load_project(project_id=project_id).get_project_genomedata(),
        media_type="text/csv",
        headers={"content-disposition": f"attachment; filename=genome_{project_id}.csv"},
    )


@app.get("/api/projects/{project_id}/heatmap/{selected_chr}/{selected_region}")
def get_heatmap(project_id: str, selected_chr: str, selected_region: str):
    print("CHR")
    print(selected_chr)
    print("Region")
    print(selected_region)
    return Response(
        load_project(project_id=project_id).get_project_local_heatmap_data(
            chromosome=selected_chr,
            region=selected_region,
        ),
        media_type="text/csv; charset=utf-8",
    )


@app.get("/api/projects/{project_id}/manhattan/{selected_chr}/{selected_region}")
def get_manhattan(project_id: str, selected_chr: str, selected_region: str):
    return Response(
        load_project(project_id=project_id).get_project_local_manhattan_data(
            chromosome=selected_chr,
            region=selected_region,
        ),
        media_type="text/csv; charset=utf-8",
    )


@app.get("/api/queue_status")
def queue_status():
    return JSONResponse(get_queue_status())




# @blp_inittable.route("")
# class InitMetaMethodView(MethodView):
#     @blp_inittable.response(200, InittableMetaSchema())
#     def get(self):
#         """Number of phenotype and SNPs"""
#         return get_inittable_meta(os.path.join(config["DATA_DIR"], "initTable.hdf5"))

# @blp_projects.route("")
# class ProjectCreateMethodView(MethodView):
#     @blp_projects.arguments(ProjectParamsSchema(), location="form")
#     @blp_projects.response(200, ProjectSchema())
#     def post(self, parameters):
#         """List projects"""
#         phenotype_ids = [
#             phenotype_id
#             for ids_with_commas in parameters["phenotypeID"]
#             for phenotype_id in ids_with_commas.split(",")
#         ]
#         phenotypes = list(filter(lambda d: d.id in phenotype_ids, get_phenotypes()))
#         return create_project([p.id for p in phenotypes], get_phenotypes())


# @blp_local_projects.route("")
# class LocalProjectCreateMethodView(MethodView):
#     @blp_projects.arguments(LocalProjectParamsSchema(), location="form")
#     @blp_projects.response(200, ProjectSchema())
#     def post(self, parameters):
#         """List projects"""
#         phenotype_ids = [
#             phenotype_id
#             for ids_with_commas in parameters["phenotypeID"]
#             for phenotype_id in ids_with_commas.split(",")
#         ]
#         phenotypes = list(filter(lambda d: d.id in phenotype_ids, get_phenotypes()))
#         return create_project(
#             [p.id for p in phenotypes],
#             get_phenotypes(),
#             str(parameters["chromosome"]),
#             str(parameters["start"]),
#             str(parameters["end"]),
#         )


# @blp_projects.route("/<project_id>")
# class ProjectDetailMethodView(MethodView):
#     @blp_projects.response(200, ProjectSchema())
#     def get(self, project_id):
#         return load_project(project_id=project_id)


# @blp_projects.route("/<project_id>/csv_status")
# class ProjectCSVStatusMethodView(MethodView):
#     def get(self, project_id):
#         return load_project(project_id=project_id).get_csv_file_generation()












# @blp_projects.route("/<project_id>/zoom_manhattan")
# class ProjectZoomManhattanMethodView(MethodView):
#     def get(self, project_id):
#         try:
#             return (
#                 load_project(project_id=project_id).get_project_local_manhattan_data(),
#                 200,
#                 {"Content-Type": "text/plain; charset=utf-8"},
#             )
#         except FileNotFoundError:
#             status = Project(id=project_id).status
#             # if status == Project.DOES_NOT_EXIST:
#             #     return (
#             #         f"project {project_id} does not exist",
#             #         404,
#             #         {"Content-Type": "text/plain; charset=utf-8"},
#             #     )
#             # elif status["worktable"] == Project.CREATING:
#             #     return (
#             #         "data not ready yet",
#             #         202,
#             #         {"Content-Type": "text/plain; charset=utf-8"},
#             #     )
#             # else:
#             #     abort(500)



# @blp_projects.route("/<project_id>/zoomplot")
# class ProjectZoomPlotMethodView(MethodView):
#     def get(self, project_id):
#         try:
#             return send_file(
#                 load_project(project_id=project_id).get_zoom_plot_path(), mimetype="image/png"
#             )
#         except FileNotFoundError:
#             status = load_project(project_id=project_id).status
#             # if status == Project.DOES_NOT_EXIST:
#             #     return (
#             #         f"project {project_id} does not exist",
#             #         404,
#             #         {"Content-Type": "text/plain; charset=utf-8"},
#             #     )
#             # elif status["worktable"] == Project.CREATING:
#             #     return (
#             #         "data not ready yet",
#             #         202,
#             #         {"Content-Type": "text/plain; charset=utf-8"},
#             #     )
#             # else:
#             #     abort(500)





# class JassApp(Flask):
#     """
#     JassApp builds the JASS Flask application
#     """

#     def __init__(self):
#         self.flask_app = Flask(__name__, static_url_path="", static_folder="static")
#         self.flask_app.config["API_TITLE"] = "JASS API"
#         self.flask_app.config["API_VERSION"] = "v2.0"
#         self.flask_app.config["OPENAPI_VERSION"] = "3.0.2"
#         self.flask_app.route("/")(self.redirect_to_index)
#         self.api = Api(self.flask_app)

#     def create_app(self):
#         return self.flask_app

#     def redirect_to_index(self):
#         return redirect("index.html")

#     def register_api_blueprint(self, blp):
#         self.api.register_blueprint(blp, url_prefix=f"/api/{blp.url_prefix}")


# jass_app = JassApp()
# jass_app.register_api_blueprint(blp_phenotypes)
# jass_app.register_api_blueprint(blp_projects)
# jass_app.register_api_blueprint(blp_local_projects)
# jass_app.register_api_blueprint(blp_inittable)
