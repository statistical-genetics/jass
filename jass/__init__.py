"""
This is the main JASS Python module

Submodules
==========

.. autosummary::
    :toctree: _autosummary

    config
    models
    server
    tasks
    util
"""

from jass.tasks import celery
