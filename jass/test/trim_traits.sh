#!/bin/bash

DATA_DIRS=`ls . -1 | grep data_`

for DATA_DIR in $DATA_DIRS; do
  if [ -f $DATA_DIR ]; then
    continue;
  fi
  echo "Doing $DATA_DIR"

  TRAITS_TAB=`ls ${DATA_DIR}/z_* -1`
  for T in $TRAITS_TAB; do
    cat $T | head -n 20 > /tmp/sub
    mv /tmp/sub $T
  done

done