# how to generate test data

## news way
```shell
 ./update_test_hdf5_files.sh
```

## old way
```shell
jass create-inittable --input-data-path "./data_test1/z*.txt" --init-covariance-path "./data_test1/COV.csv" --init-genetic-covariance-path "./data_test1/COV.csv" --regions-map-path "./data_test1/regions.txt" --description-file-path "./data_test1/summary.csv" --init-table-path "./data_test1/initTable.hdf5"

jass create-inittable --input-data-path "./data_test2/z*.txt" --init-covariance-path "./data_test2/COV.csv" --init-genetic-covariance-path "./data_test2/COV.csv" --regions-map-path "./data_test2/regions.txt" --description-file-path "./data_test2/summary.csv" --init-table-path "./data_test2/initTable.hdf5"

jass create-project-data --init-table-path data_test1/initTable.hdf5 --phenotype z_DISNEY_POCAHONT z_DISNEY_RATATOUY --worktable-path ./data_test1/worktable.hdf5

jass create-project-data --init-table-path data_test2/initTable.hdf5 --phenotype z_BMW_ISETTA z_BMW_MINI z_FIAT_CINQCENT z_FIAT_CINQUECENTO z_MERCO_SMART z_TATA_TATANANO --worktable-path ./data_test2/worktable.hdf5

```