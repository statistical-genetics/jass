#!/bin/bash

DATA_DIRS=`ls . -1 | grep data_`

for DATA_DIR in $DATA_DIRS; do
  if [ -f $DATA_DIR ]; then
    continue;
  fi
  echo "Doing $DATA_DIR"

  TRAITS_TAB=`ls ${DATA_DIR}/z_* -1 | cut -d"/" -f2 | cut -d"_" -f1-3 | sort|uniq`
  TRAITS=""
  for T in $TRAITS_TAB; do
    TRAITS="${TRAITS} $T";
  done

  echo "Found traits $TRAITS"

  GEN_COV="./${DATA_DIR}/GEN_COV.csv"
  if [ ! -f $GEN_COV ]; then
    echo "GEN_COV not found, fallback on COV as GEN_COV"
    GEN_COV="./${DATA_DIR}/COV.csv";
  fi

  echo "Creating inittable"
  jass create-inittable --input-data-path "./${DATA_DIR}/z*.txt" --init-covariance-path "./${DATA_DIR}/COV.csv" --init-genetic-covariance-path ${GEN_COV} --regions-map-path "./${DATA_DIR}/regions.txt" --description-file-path "./${DATA_DIR}/summary.csv" --init-table-metadata-path "./${DATA_DIR}/metadata.txt" --init-table-path "./${DATA_DIR}/initTable.hdf5"

  echo "Creating inittable without pre-computed covariance matrix"
  jass create-inittable --input-data-path "./${DATA_DIR}/z*.txt"                                                --init-genetic-covariance-path ${GEN_COV} --regions-map-path "./${DATA_DIR}/regions.txt" --description-file-path "./${DATA_DIR}/summary.csv" --init-table-metadata-path "./${DATA_DIR}/metadata.txt" --init-table-path "./${DATA_DIR}/initTable-computed-cov.hdf5"

  echo "Creating worktable with Nan"
  jass create-project-data --init-table-path "${DATA_DIR}/initTable.hdf5" --phenotype ${TRAITS} --worktable-path ./${DATA_DIR}/worktable-withnans.hdf5

  echo "Creating worktable"
  jass create-project-data --init-table-path "${DATA_DIR}/initTable.hdf5" --phenotype ${TRAITS} --worktable-path ./${DATA_DIR}/worktable-nonans.hdf5 --remove-nans

done