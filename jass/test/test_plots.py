# coding: utf-8

from __future__ import absolute_import
import os, shutil, tempfile
from pathlib import Path
import matplotlib as plt
from jass.models import plots
from PIL import Image
import numpy as np

from . import JassTestCase


class TestPlots(JassTestCase):

    test_folder = "data_real"
    expected_res_folder = "expected_graphs"
    #expected_res_folder="baseline_images/test_plot"

    def setUp(self):
        # Create a temporary directory
        self.test_dir = Path(tempfile.mkdtemp())
        self.ref_res_dir=Path(os.path.join(os.path.dirname(os.path.abspath(__file__)), self.expected_res_folder))
        self.worktable_hdf_path = self.get_file_path_fn("worktable-withnans.hdf5")

    def tearDown(self):
        # Remove the directory after the test
        shutil.rmtree(self.test_dir)
        pass

    def test_create_global_plot(self):
        #import shutil
        #print(plt.rcParams)
        plots.create_global_plot(self.worktable_hdf_path, self.test_dir / "global_plot.png")
        img_new=Image.open(self.test_dir /"global_plot.png")
        img_ref=Image.open(self.ref_res_dir / "expected_global_plot.png")
        sum_sq_diff = np.sum((np.asarray(img_new).astype('float') - np.asarray(img_ref).astype('float')) ** 2)
        print("sum_sq_diff=",sum_sq_diff)
        assert(sum_sq_diff==0.0)


    def test_create_qq_plot(self):
        plots.create_qq_plot(self.worktable_hdf_path, self.test_dir / "qq_plot.png")

    def test_create_quadrant_plot(self):
        plots.create_quadrant_plot(self.worktable_hdf_path, self.test_dir / "quadrant_plot.png")

    def test_create_qq_plot_by_GWAS(self):
        plots.create_qq_plot_by_GWAS(self.worktable_hdf_path, self.test_dir)


if __name__ == "__main__":
    import unittest

    unittest.main()
