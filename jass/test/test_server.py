# coding: utf-8

from __future__ import absolute_import
import os, shutil, tempfile

from six import BytesIO
from flask import json, url_for

from jass.config import config
from . import JassWebClientTestCase


class TestDefaultController(JassWebClientTestCase):
    """DefaultController integration test stubs"""

    test_folder = "data_real"

    def test_phenotypes_post(self):
        """
        Test case retrieving available phenotypes
        """
        response = self.testing_client.post("/api/phenotypes", json={})
        self.assert200(response, "Response body is : " + response.content.decode("utf-8"))

        response = self.testing_client.post(
            "/api/phenotypes",
            json={"initTableName": "initTable.hdf5"},
        )
        self.assert200(response, "Response body is : " + response.content.decode("utf-8"))
        json_response_main = json.loads(response.content.decode("utf-8"))
        phenotypes_main = set(p["id"] for p in json_response_main)

        response = self.testing_client.post(
            "/api/phenotypes",
            json={"initTableName": "initTableTest1.hdf5"},
        )
        self.assert200(response, "Response body is : " + response.content.decode("utf-8"))
        json_response_t1 = json.loads(response.content.decode("utf-8"))
        phenotypes_t1 = set(p["id"] for p in json_response_t1)

        self.assertNotEqual(json_response_t1, json_response_main)
        self.assertNotEqual(phenotypes_main, phenotypes_t1)
        self.assertEqual(phenotypes_main.intersection(phenotypes_t1), set())

        response = self.testing_client.post(
            "/api/phenotypes",
            json={"initTableName": "initTableMissing.hdf5"},
        )
        self.assertEqual(response.status_code, 404, response.content.decode("utf-8"))

    def test_create_project(self):
        """
        Test case for creating a project
        """
        response = self.testing_client.post(
            "/api/projects",
            json={"phenotypeID": ["z_MAGIC_FAST-GLUCOSE"]},
        )
        self.assert200(response, "Response body is : " + response.content.decode("utf-8"))

    def test_initmeta(self):
        response = self.testing_client.post("/api/initmeta", json={})
        self.assert200(response, "Response body is : " + response.content.decode("utf-8"))
        respMain = json.loads(response.content.decode("utf-8"))

        response = self.testing_client.post("/api/initmeta", json={"initTableName": "initTableTest1.hdf5"})
        self.assert200(response, "Response body is : " + response.content.decode("utf-8"))
        respT1 = json.loads(response.content.decode("utf-8"))

        self.assertNotEqual(respT1, respMain)
        for key in {
            'nb_phenotypes',
            'nb_snps',
            'name',
            'desc',
            'ancestry',
            'assembly',
        }:
            self.assertIn(key , respMain)

    def test_get_tables(self):
        response = self.testing_client.get("/api/tables")
        self.assert200(response, "Response body is : " + response.content.decode("utf-8"))
        resp = json.loads(response.content.decode("utf-8"))
        self.assertSetEqual({"initTable.hdf5", "initTableTest1.hdf5"}, set(resp))


if __name__ == "__main__":
    import unittest

    unittest.main()
