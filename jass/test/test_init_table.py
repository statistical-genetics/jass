# coding: utf-8

from __future__ import absolute_import
import os, shutil, tempfile

from pandas.testing import assert_frame_equal
from pandas import read_hdf

from jass.models.inittable import create_inittable_file

from . import JassTestCase


class TestInitTable(object):
    test_folder = "data_real"
    __test__ = False

    def setUp(self):
        # Create a temporary directory
        self.test_dir = tempfile.mkdtemp()
        input_data_path = self.get_file_path_fn("*chr*.txt")
        if self.computed_cov:
            init_covariance_path = None
        else:
            init_covariance_path = self.get_file_path_fn("COV.csv")
        regions_map_path = self.get_file_path_fn("regions.txt")
        description_file_path = self.get_file_path_fn("summary.csv")
        self.expected_hdf_path = self.get_file_path_fn(f"initTable{'-computed-cov' if self.computed_cov else ''}.hdf5")
        init_table_path = os.path.join(self.test_dir, "test_init_table.hdf5")
        create_inittable_file(
            input_data_path,
            regions_map_path,
            description_file_path,
            init_table_path,
            init_covariance_path,
        )
        self.expected_phenolist = read_hdf(self.expected_hdf_path, "PhenoList")
        self.expected_sum_stat_tab = read_hdf(self.expected_hdf_path, "SumStatTab")
        self.expected_cov = read_hdf(self.expected_hdf_path, "COV")
        self.result_phenolist = read_hdf(init_table_path, "PhenoList")
        self.result_sum_stat_tab = read_hdf(init_table_path, "SumStatTab")
        self.result_cov = read_hdf(init_table_path, "COV")

    def tearDown(self):
        # Remove the directory after the test
        # shutil.rmtree(self.test_dir)
        print(self.test_dir)

    def test_compare_phenolist(self):
        """
        Compare result and expected PhenoList
        """
        assert_frame_equal(
            self.expected_phenolist, self.result_phenolist, check_like=True
        )

    def test_compare_sumstattab(self):
        """
        Compare result and expected SumStatTab
        """
        assert_frame_equal(
            self.expected_sum_stat_tab, self.result_sum_stat_tab, check_like=True
        )

    def test_compare_cov(self):
        """
        Compare result and expected COV
        """
        assert_frame_equal(self.expected_cov, self.result_cov, check_like=True)


class TestInitTableNoNans(TestInitTable, JassTestCase):
    computed_cov = True


class TestInitTableWithNans(TestInitTable, JassTestCase):
    computed_cov = False


if __name__ == "__main__":
    import unittest

    unittest.main()
