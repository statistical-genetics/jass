import unittest
import logging
import os, shutil, tempfile
import unittest


# replace delay() and si() method with mocks
# to avoid freezing calls in unit tests
from celery.app.task import Task
from unittest.mock import MagicMock
from fastapi.testclient import TestClient


Task.delay = MagicMock()
Task.si = MagicMock()

from jass.models.inittable import create_inittable_file


class JassTestCase(unittest.TestCase):
    @classmethod
    def get_file_path_fn(cls, test_file_name):
        return os.path.join(
            os.path.dirname(os.path.abspath(__file__)), cls.test_folder, test_file_name
        )

class JassWebClientTestCase(JassTestCase):
    def setUp(self) -> None:
        from jass.config import config
        self.test_dir = tempfile.mkdtemp()
        config["DATA_DIR"] = self.test_dir
        shutil.copy(self.get_file_path_fn("initTable.hdf5"), self.test_dir)
        try:
            shutil.copy(self.get_file_path_fn("initTableTest1.hdf5"), self.test_dir)
        except FileNotFoundError:
            pass

        from jass.server import app
        self.testing_client = TestClient(app)

    def assert200(self, response, message):
        self.assertEqual(response.status_code, 200, message)

    # def create_app(self):
    #     from jass.config import config
    #
    #     self.test_dir = tempfile.mkdtemp()
    #     config["DATA_DIR"] = self.test_dir
    #     shutil.copy(self.get_file_path_fn("initTable.hdf5"), self.test_dir)
    #
    #     self.jass_app = jass_app
    #     application = self.jass_app.create_app()
    #     application.config["TESTING"] = True
    #     self.testing_client = application.test_client()
    #     return application

    def tearDown(self):
        shutil.rmtree(self.test_dir)
