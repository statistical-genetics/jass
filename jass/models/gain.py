import pkg_resources
import pandas as pd
import numpy as np
import os

# data issued from https://doi.org/10.1101/2023.10.27.564319
stream = pkg_resources.resource_stream(__name__, 'data/range_feature_gain_prediction.tsv')
X_range = pd.read_csv(stream, sep="\t", index_col=0)

stream = pkg_resources.resource_stream(__name__, 'data/coef_mean_model.tsv')
model_coefficients =  pd.read_csv(stream, sep="\t", index_col=0)

# Scale according to observed 
def scale_feature(X, feature_name):
    X_std = (X - X_range.loc[feature_name, "minimum_value"]) / ( X_range.loc[feature_name, "maximum_value"] -  X_range.loc[feature_name, "minimum_value"])
    return X_std

def preprocess_feature(df_combinations):
    # transformation of features
 
    df_combinations['log10_mean_gencov'] = np.log10(df_combinations.mean_gencov)
    df_combinations['log10_avg_distance_cor'] = np.log10(df_combinations.avg_distance_cor)
    for f in ["k", "log10_avg_distance_cor", "log10_mean_gencov", "avg_Neff", "avg_h2", "avg_perc_h2_diff_region"]:
        df_combinations[f] = scale_feature(df_combinations[f], f)
    return df_combinations

def compute_gain(df_combinations, path_output):

    preprocess_feature(df_combinations)
    df_combinations["gain"] = df_combinations[["k", "log10_avg_distance_cor", "log10_mean_gencov", "avg_Neff", "avg_h2", "avg_perc_h2_diff_region"]].dot(model_coefficients["0"].values)
    df_combinations.sort_values(by="gain", ascending=False).to_csv(path_output, sep="\t")

# cov to cor
def cov2cor(c):
    """
    Return a correlation matrix given a covariance matrix. 
    : c = covariance matrix
    """
    D = 1 / np.sqrt(np.diag(c)) # takes the inverse of sqrt of diag.
    return D * c * D

def compute_detected_undected_h2(inittable_path): 

    phenoL = pd.read_hdf(inittable_path, "PhenoList")
    gen_cov = pd.read_hdf(inittable_path, "GEN_COV")
    region = pd.read_hdf(inittable_path, "Regions")

    combi_c = list(phenoL.index)
    reg_start=0
    reg_end=50

    chunk_size=50
    Nchunk = region.shape[0] // chunk_size + 1
    start_value = 0
    
    zscore_threshold = 5.452
    h2_GW = np.zeros(len(combi_c))

    for chunk in range(start_value, Nchunk):
        print(chunk)
        binf = chunk * chunk_size
        bsup = (chunk + 1) * chunk_size

        init_extract = pd.read_hdf(inittable_path, "SumStatTab", where= "Region >= {0} and Region < {1}".format(reg_start, reg_end))

        init_extract[combi_c] = init_extract[combi_c].abs()
        max_zscore = init_extract[["Region"] + combi_c].groupby("Region").max()

        Neff_term = np.ones(max_zscore.shape)
        Neff_term = Neff_term* (1/phenoL.loc[combi_c, "Effective_sample_size"].values)

        beta_2 = max_zscore.mask((max_zscore < zscore_threshold)) 
        beta_2 = beta_2 * np.sqrt(Neff_term)
        beta_2 = beta_2.mask( (beta_2 > 0.019))

        h2_GW += (beta_2**2).sum()

    h2 = np.diag(gen_cov.loc[combi_c, combi_c])
    undetected_h2 = ((h2 - h2_GW) / h2)

    phenoL["h2"] = h2
    phenoL["h2_GW"] = h2_GW
    phenoL["undetected_h2_perc"] = undetected_h2
    
    return phenoL

def add_h2_to_pheno_description(inittable_path):
    phenoL_before = pd.read_hdf(inittable_path, "PhenoList")
    if "avg_perc_h2_diff_region" in phenoL_before.columns:    
        phenoL = compute_detected_undected_h2(inittable_path)
        phenoL.to_hdf(inittable_path, key="table")


def compute_mean_cov(cov, combi_c):
    rows, cols = np.indices(cov.loc[combi_c, combi_c].shape)
    mean_gencov = cov.loc[combi_c, combi_c].where(rows != cols).stack().abs().mean()
    return mean_gencov

def compute_diff_cor(res_cov, gen_cov, combi_c):
    res_cor = cov2cor(res_cov.loc[combi_c, combi_c])
    gen_cor = cov2cor(gen_cov.loc[combi_c, combi_c])
    rows, cols = np.indices(res_cor.loc[combi_c, combi_c].shape)
    off_gencor = res_cor.where(rows != cols).stack()
    off_rescor = gen_cor.where(rows != cols).stack()
    return (off_gencor - off_rescor).abs().mean()

def compute_mean_undetected_h2(phenoL, combi_c):
    mean_h2 = np.mean(phenoL.loc[combi_c, "undetected_h2_perc"])
    return mean_h2

def compute_mean_h2(phenoL, combi_c):
    mean_h2 = np.mean(phenoL.loc[combi_c, "h2"])
    return mean_h2

def compute_mean_Neff(phenoL, combi_c):
    mean_neff = np.mean(phenoL.loc[combi_c, "Effective_sample_size"])
    return mean_neff


#beta_2_GW = beta_2_GW * (1/phenoL.loc[combi_c, "Effective_sample_size"].values)
def create_features(inittable_path, combi_file):
    add_h2_to_pheno_description(inittable_path)

    phenoL = pd.read_hdf(inittable_path, "PhenoList")
    gen_cov = pd.read_hdf(inittable_path, "GEN_COV")
    res_cov = pd.read_hdf(inittable_path, "COV")

    combi = pd.read_csv(combi_file, sep=",", index_col=0, names=["combi"])
    combi = list(combi.combi.str.split(" "))

    D = {'traits':[] ,'k':[], 'avg_distance_cor': [], 'mean_gencov': [], 
        'avg_Neff':[], 'avg_h2':[], 'avg_perc_h2_diff_region':[]}

    for c in combi:
        D['traits'].append(str(c))
        D["k"].append(len(c))
        D["avg_distance_cor"].append(compute_diff_cor(res_cov, gen_cov, c))
        D["mean_gencov"].append(compute_mean_cov(gen_cov, c))
        D["avg_Neff"].append(compute_mean_Neff(phenoL, c))

        D["avg_h2"].append(compute_mean_h2(phenoL, c))
        D["avg_perc_h2_diff_region"].append(compute_mean_undetected_h2(phenoL, c))

    return pd.DataFrame.from_dict(D)

