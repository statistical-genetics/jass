# coding: utf-8
"""
contains all the code managing the various data manipulation operations

Submodules
==========

.. autosummary::
    :toctree: _autosummary

    inittable
    phenotype
    plots
    project
    stats
    worktable
"""

from __future__ import absolute_import

# import models into model package
from .phenotype import Phenotype
