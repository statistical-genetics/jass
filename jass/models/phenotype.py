# coding: utf-8
from typing import List, Optional

import os
import pandas
from pydantic import BaseModel, validator

from jass.config import config
from jass.models.inittable import valideInitTableNamePattern


class Phenotype(BaseModel):
    """
    Phenotype - a model defined in Swagger

    :param id: dataset ID.
    :type id: str
    :param consortium: dataset consortium.
    :type consortium: str
    :param outcome: dataset outcome
    :type outcome: str
    :param full_name: dataset full name
    :type full_name: str
    :param typ: dataset type
    :type typ: str
    :param ref: dataset bibliographic reference
    :type ref: str
    :param ref_link: dataset bibliographic reference link
    :type ref_link: str
    :param data_link: dataset link
    :type data_link: str
    :param data_path: dataset path
    :type data_path: str
    """

    id: str
    consortium: str
    outcome: str
    full_name: str
    typ: str
    ref: str
    ref_link: str
    data_link: str
    data_path: str

    @classmethod
    def from_phenolist(cls, row) -> "Phenotype":
        """
        Returns the PhenoList row as a Phenotype object

        :param row: A "PhenoList" dataframe row, as a dict.
        :type: dict
        :return: The generated PhenoList.
        :rtype: PhenoList
        """
        p = cls(
            id=row["ID"],
            consortium=row["Consortium"],
            outcome=row["Outcome"],
            full_name=row["FullName"],
            typ=row["Type"],
            ref=row["Reference"],
            ref_link=row["ReferenceLink"],
            data_link=row["dataLink"],
            data_path=row["internalDataLink"],
        )
        return p


def get_available_phenotypes(init_file_path: str):
    phenolist_dataframe = pandas.read_hdf(init_file_path, "PhenoList")
    phenotypes = []
    for index, row in phenolist_dataframe.iterrows():
        phenotype = Phenotype.from_phenolist(row)
        phenotypes.append(phenotype)
    return phenotypes


class InitTableNameModel(BaseModel):
    initTableName: Optional[str] = "initTable.hdf5"

    @validator("initTableName")
    def validate_path_injection(cls, value):
        if not valideInitTableNamePattern.match(value):
            raise ValueError(f"Prohibited char, only \"{valideInitTableNamePattern.pattern}\" allowed.")
        return value

    def get_init_table_path(self):
        return os.path.join(config["DATA_DIR"], self.initTableName)

class PhenotypeIdList(InitTableNameModel, BaseModel):
    phenotypeID: List[str] = []
