# -*- coding: utf-8 -*-
"""
This contains all functions for accessing the "worktable" hdf5 file.
All functions either create or read a worktable at a specific path location.

@author: vguillem, hmenager, hjulienne
"""
import math

from jass.models.stats import (
    make_stat_computer_nopattern,
    make_stat_computer_pattern,
    make_stat_computer_pattern_big,
    make_stat_computer_nan_dumb,
)

from jass.config import config
import logging
import os
import importlib

from typing import List


from pandas import HDFStore, DataFrame, concat, read_hdf, read_csv, Series, Index

import numpy as np
import scipy.stats as spst
import tables
import warnings
import json

warnings.filterwarnings("ignore", category=tables.NaturalNameWarning)


def signif(x, digit):
    """
    signif
    Round a number x to represent it with <digit> digits

    :param x: the number to round
    :type x: float

    :param digit: the number of digits
    :type digit: int

    :return: the rounded number
    :rtype: float

    example:
    >>> signif(1.2345678, 1)
    1.0

    >>> signif(1.2345678, 3)
    1.23

    >>> signif(1.2345678, 5)
    1.2346
    """
    if x == 0:
        return 0

    return round(x, digit - int(math.floor(math.log10(abs(x)))) - 1)


def choose_stat_function(
    smart_na_computation, optim_na, big, function_name, stat_function, sub_cov, **kwargs
):
    if smart_na_computation:
        # If stat is sumz use normal computer even with na
        if function_name == "omnibus_stat":
            if optim_na:
                if big:
                    stat_compute = make_stat_computer_pattern_big(
                        sub_cov, stat_function
                    )
                else:
                    stat_compute = make_stat_computer_pattern(sub_cov, stat_function)
            else:
                stat_compute = make_stat_computer_nan_dumb(sub_cov, stat_function)
        else:
            if function_name == "meta_analysis":
                stat_compute = make_stat_computer_nopattern(
                    sub_cov, stat_function, **kwargs
                )
            elif function_name == "sumz_stat":
                loading_file = kwargs.get("loadings", None)
                if loading_file is None:
                    # Default loadings would be one for every phenotypes
                    stat_compute = make_stat_computer_nopattern(sub_cov, stat_function)
                else:
                    loadings = read_csv(loading_file, index_col=0)
                    loadings = loadings.iloc[:, 0]
                    stat_compute = make_stat_computer_nopattern(
                        sub_cov, stat_function, loadings=loadings
                    )
            else:
                stat_compute = make_stat_computer_nopattern(sub_cov, stat_function)
    else:
        stat_compute = make_stat_computer_nopattern(sub_cov, stat_function)

    return stat_compute


def add_signif_status_column(region_sub_tab, significance_treshold=5 * 10 ** -8):

    region_sub_tab["signif_status"] = ""

    # blue: significant pvalues for omnibus and univariate tests
    cond = np.where(
        (region_sub_tab.JASS_PVAL < significance_treshold)
        & (region_sub_tab.UNIVARIATE_MIN_PVAL < significance_treshold)
    )[0]
    region_sub_tab.loc[region_sub_tab.index[cond], "signif_status"] = "Both"

    # red: significant pvalues for omnibus test only
    cond = np.where(
        (region_sub_tab.JASS_PVAL < significance_treshold)
        & (region_sub_tab.UNIVARIATE_MIN_PVAL > significance_treshold)
    )[0]
    region_sub_tab.loc[region_sub_tab.index[cond], "signif_status"] = "Joint"

    # green: significant pvalues for univariate test only
    cond = np.where(
        (region_sub_tab.JASS_PVAL > significance_treshold)
        & (region_sub_tab.UNIVARIATE_MIN_PVAL < significance_treshold)
    )[0]
    region_sub_tab.loc[region_sub_tab.index[cond], "signif_status"] = "Univariate"

    # grey: non significant pvalues
    cond = np.where(
        (region_sub_tab.JASS_PVAL > significance_treshold)
        & (region_sub_tab.UNIVARIATE_MIN_PVAL > significance_treshold)
    )[0]
    region_sub_tab.loc[region_sub_tab.index[cond], "signif_status"] = "None"

    return region_sub_tab



def get_region_summary(sum_stat_tab, phenotype_ids, significance_treshold=5 * 10 ** -8):

    # Select the most significant SNP for the joint test for each region
    region_sub_tab = (
        sum_stat_tab.sort_values("JASS_PVAL").groupby("Region").first()
    )  # .reset_index()

    # add minimum univariate p-value
    univar = sum_stat_tab.groupby("Region").min().UNIVARIATE_MIN_PVAL
    region_sub_tab.loc[univar.index, "UNIVARIATE_MIN_PVAL"] = univar.values

    # Tag SNPs depending on which test is significant
    region_sub_tab.reset_index(inplace=True)
    region_sub_tab = add_signif_status_column(region_sub_tab, significance_treshold)

    #  reorder columns
    region_sub_tab = region_sub_tab[
        [
            "Region",
            "MiddlePosition",
            "snp_ids",
            "CHR",
            "position",
            "Ref_allele",
            "Alt_allele",
            "JASS_PVAL",
            "UNIVARIATE_MIN_PVAL",
            "signif_status",
        ]
        + phenotype_ids
    ]

    return region_sub_tab


def post_computation_filtering(worktable_chunk, significant_treshold=5 * 10 ** -8):
    """
    Remove SNPs that seems aberrant: SNPs with a very low p-value that are isolated
     in their region

    :param worktable_chunk: pandas DataFrame worktable part
    :type worktable_chunk: pandas DataFrame
    :param significant_treshold: threshold at which a p-value is considered significant
    :type significant_treshold: float
    """

    def count_nearly_significant(rsnp):
        return (rsnp.JASS_PVAL < (significant_treshold * 20)).sum()

    res = worktable_chunk.groupby("Region").apply(count_nearly_significant)

    # select region with only one SNP that is significant which is
    # suspect
    reg = res.loc[res == 1].index

    for reg_aberant in reg:
        aberant_SNP = (
            worktable_chunk.loc[worktable_chunk.Region == reg_aberant]
            .sort_values("JASS_PVAL")
            .index[0]
        )
        worktable_chunk.drop(aberant_SNP, inplace=True)

    return worktable_chunk


def compute_pleiotropy_index(W, significance_treshold):

    N_significatif = (
        2.0 * spst.norm.sf(W.fillna(0, inplace=False).abs()) < significance_treshold
    ).sum(1)
    N_pheno = (~W.isnull()).sum(1)
    # pleiotropy index is not meaningful for too few phenotype
    S = N_significatif / N_pheno
    S.loc[N_pheno < 4] = np.nan
    return S


def create_worktable_file(
    phenotype_ids: List[str],
    init_file_path: str,
    project_hdf_path: str,
    remove_nan: bool,
    stat: str = "jass.models.stats:omnibus_stat",
    optim_na: bool = True,
    csv_file: str = None,
    chunk_size: int = 50,
    significance_treshold=5 * 10 ** -8,
    post_filtering=True,
    delayed_gen_csv_file=False,
    chromosome: str = None,
    pos_Start: str = None,
    pos_End: str = None,
    callback_report= None,
    **kwargs
):

    """
    Create a worktable file from an initial data table by specifying the
    selected phenotypes and the computation strategy

    :param phenotype_ids: the list of IDs for the phenotypes to select
    :type phenotype_ids: list
    :param init_file_path: path to the initial data table
    :type init_file_path: str
    :param project_hdf_path: path to the worktable file that will be produced
    :type project_hdf_path: str
    :param remove_nan: boolean to control the JOST computation strategy:
                      - if True any SNP which has a NaN value for one of its \
                        phenotypes will be removed.
                      - if False SNPs will be removed only if all phenotype \
                        values are NaN and JOST will be performed the "smart" \
                        way to compensate these missing values.
    :type remove_nan: bool
    :param optim_na: boolean to control if we use a smart gestion of z NA \
    values. Should always be set to True except for performance tests.
    :type optim_na: bool
    :param chunk_size: the size of the chunks of the initial data
                       to process together. default is 50 and should probably not
                       be touched for anything else than experimentation
    :type chunk_size: int
    :param significant_treshold: threshold at which a p-value is considered significant
    :type significant_treshold: float
    :param post_filtering: Remove SNPs that seems aberrant
    :type post_filtering: bool
    :param delayed_gen_csv_file: generate the csv_file asynchronously (used in the web interface)
    :type delayed_gen_csv_file: bool
    :param chromosome: Chromosome number selected for local analysis
    :type chromosome: str
    :param pos_Start: start of the position of the studied region (base point) for local analysis
    :type pos_Start: str
    :param pos_End: end of the position of the studied region (base point) for local analysis
    :type pos_End: str

    :return: Number of chunks used to write workTable file. \
            This information is useful for reading the file
    :rtype: int
    """
    # number of phenotypes beyond which we change the algorithm
    K_NB_PHENOTYPES_BIG = 18

    # Upper bound of the chromosome length (bp)
    K_POS_MAX = 250000000

    # Minimum and maximum limit of regions for each chromosome (multiples of 50)
    Min_pos_chr = [
        0,
        100,
        250,
        400,
        500,
        600,
        700,
        800,
        900,
        1000,
        1050,
        1150,
        1250,
        1300,
        1350,
        1400,
        1450,
        1500,
        1550,
        1600,
        1650,
        1650,
    ]
    Max_pos_chr = [
        150,
        300,
        400,
        550,
        650,
        750,
        850,
        950,
        1050,
        1100,
        1200,
        1300,
        1350,
        1400,
        1450,
        1500,
        1550,
        1600,
        1650,
        1700,
        1700,
        1750,
    ]

    N_pheno = len(phenotype_ids)

    # Controls the number of phenotypes
    if (((N_pheno > 64) & (stat == "jass.models.stats:omnibus_stat")) & (remove_nan == False)):
        print(
            "ERROR: {} phenotypes are selected. The current version of JASS cannot analyze more than 64 phenotypes for the Omnibus Test when keeping missing values. Try to set remove_nan=TRUE to analyse only complete cases or select another test".format(
                N_pheno
            )
        )
        raise ValueError("Maximum number of phenotypes exceeded")
    elif N_pheno >= 20:
        print(
            "WARNING: {} phenotypes are selected. The computation might take a while".format(
                N_pheno
            )
        )

    if chromosome is None:
        local_analysis = False
        print("============== Whole genome analysis ===============")
    else:
        local_analysis = True
        print("============== Local analysis ===============")
        if not (chromosome.isdigit()):
            print(
                "ERROR: when performing a local analysis, the chromosome number (between 1 and 22) is mandatory"
            )
            raise ValueError(
                "create_worktable_file: the required argument chromosome is not a number"
            )
        else:
            num_Chr = int(chromosome)

        if (pos_Start is None) and (pos_End is None):
            chromosome_full = True
            print("------ Chromosome : {} ------".format(num_Chr))
        else:
            chromosome_full = False
            if (pos_Start is None) or (not pos_Start.isdigit()):
                pos_Start = 0
            if (pos_End is None) or (not pos_End.isdigit()):
                pos_End = K_POS_MAX
            print(
                "------ Chromosome : {} ({} - {}) ------".format(
                    num_Chr, pos_Start, pos_End
                )
            )

    print("Phenotypes = {}".format(phenotype_ids))

    # select only rows (SNPs) where there are no missing data
    how_dropna = "any" if remove_nan else "all"

    if os.path.exists(project_hdf_path):
        os.remove(project_hdf_path)
    hdf_work = HDFStore(project_hdf_path)

    if csv_file is not None:
        if os.path.exists(csv_file):
            os.remove(csv_file)

        if delayed_gen_csv_file:
            create_genome_full_csv_lock_file(project_hdf_path)

    # subset of phenotypes that have been selected
    phenolist = read_hdf(init_file_path, "PhenoList")
    phenolist = phenolist.loc[phenotype_ids]
    hdf_work.put("PhenoList", phenolist)

    # subset of covariance matrix for the selected phenotypes
    cov = read_hdf(init_file_path, "COV")
    sub_cov = cov.loc[phenotype_ids, phenotype_ids]
    # Covariance matrix
    hdf_work.put("COV", sub_cov, format="table", data_columns=True)

    # If available extract genetic covariance
    try:
        gcov = read_hdf(init_file_path, "GEN_COV")
        sub_gcov = gcov.loc[phenotype_ids, phenotype_ids]
        # Covariance matrix
        hdf_work.put("GEN_COV", sub_gcov, format="table", data_columns=True)
    except KeyError:
        print("Genetic correlation not available in inittable. ")

    regions = read_hdf(init_file_path, "Regions").index.tolist()
    sum_stat_tab_min_itemsizes = {
        "snp_ids": 310,
        "Region": 10,
        "CHR": 5,
        "Ref_allele": 300,
        "Alt_allele": 300,
    }
    region_sub_table_min_itemsizes = {
        "Region": 10,
        "index": 10,
        "CHR": 5,
        "snp_ids": 310,
        "signif_status": 20,
        "Ref_allele": 300,
        "Alt_allele": 300,
    }

    smart_na_computation = not (remove_nan)
    module_name, function_name = stat.split(":")
    stat_module = importlib.import_module(module_name)
    stat_fn = getattr(stat_module, function_name)

    if N_pheno < K_NB_PHENOTYPES_BIG:
        big = False
        sub_cov_matrix = sub_cov
    else:
        big = True
        sub_cov_matrix = sub_cov.to_numpy()

    stat_compute = choose_stat_function(
        smart_na_computation,
        optim_na,
        big,
        function_name,
        stat_fn,
        sub_cov_matrix,
        samp_size=phenolist["Effective_sample_size"],
        **kwargs
    )

    # read data by chunks to optimize memory usage
    if not local_analysis:
        Nchunk = len(regions) // chunk_size + 1
        start_value = 0
    else:
        chunk_size = 50
        Nchunk = Max_pos_chr[num_Chr - 1] // chunk_size
        start_value = Min_pos_chr[num_Chr - 1] // chunk_size

        # selection criterion in the case of a partial analysis by chromosome and position
        if chromosome_full:
            Local_criteria = "(CHR == {})".format(chromosome)
        else:
            Local_criteria = (
                "(CHR == {}) and (position >= {}) and (position <= {})".format(
                    chromosome, pos_Start, pos_End
                )
            )

    Nsnp_total = 0
    Nsnp_jassed = 0

    for chunk in range(start_value, Nchunk):

        binf = chunk * chunk_size
        bsup = (chunk + 1) * chunk_size

        sum_stat_tab = read_hdf(
            init_file_path,
            "SumStatTab",
            columns=[
                "Region",
                "CHR",
                "position",
                "snp_ids",
                "Ref_allele",
                "Alt_allele",
                "MiddlePosition",
            ]
            + phenotype_ids,
            where="Region >= {0} and Region < {1}".format(binf, bsup),
        )

        print("Regions {0} to {1}\r".format(binf, bsup))

        if local_analysis:
            # Data extraction in the case of a partial analysis
            sum_stat_tab.query(Local_criteria, inplace=True)

        # Remake row index unique: IMPORTANT for assignation with .loc
        sum_stat_tab.dropna(axis=0, subset=phenotype_ids, how=how_dropna, inplace=True)
        sum_stat_tab.reset_index(drop=True, inplace=True)

        if sum_stat_tab.shape[0] == 0:
            print("No data available for region {0} to region {1}".format(binf, bsup))
            continue  # skip region if no data are available

        Nsnp_total = Nsnp_total + sum_stat_tab.shape[0]

        if remove_nan or stat.split(":")[-1] != "omnibus_stat":
            sum_stat_tab["JASS_PVAL"] = stat_compute(sum_stat_tab[phenotype_ids])
        else:
            if not big:
                # Algorithm optimized for a small number of phenotypes

                # Sort SumStatTab by missing patterns
                patterns_missing, frequent_pattern = compute_frequent_missing_pattern(
                    sum_stat_tab[phenotype_ids]
                )

                sum_stat_tab["patterns_missing"] = patterns_missing
                z1 = sum_stat_tab[phenotype_ids]

                # Apply the statistic computation by missing patterns
                for pattern in frequent_pattern:
                    bool_serie = patterns_missing == pattern
                    Selection_criteria = sum_stat_tab["patterns_missing"] == pattern

                    try:
                        sum_stat_tab.loc[bool_serie, "JASS_PVAL"] = stat_compute(
                            z1[Selection_criteria], pattern
                        )
                    except ValueError:
                        print("worktable")

            else:
                # Algorithm optimized for a high number of phenotypes

                # Sort SumStatTab by missing patterns
                (
                    patterns_missing,
                    frequent_pattern,
                    dico_index_y,
                ) = compute_frequent_missing_pattern_Big(sum_stat_tab[phenotype_ids])

                sum_stat_tab["index"] = sum_stat_tab.index.tolist()
                sum_stat_tab["patterns_missing"] = patterns_missing

                # In our case, the "apply" function transforms all numeric columns into float 64-bit encoded columns.
                # However, the mantissa of a float is encoded on 52 bits while we use an integer code using 64 bits.
                # Beyond 52 phenotypes, the automatic transformation integer-float induces a false pattern code.
                # Transforming our code into a string keeps the coding accuracy beyond 52 phenotypes up to 64 phenotypes.
                sum_stat_tab = sum_stat_tab.astype({"patterns_missing": str})

                Liste_colonnes = ["index", "patterns_missing"] + phenotype_ids
                dico_z = {}
                dico_index_x = {}

                sum_stat_tab[Liste_colonnes].apply(
                    lambda x: store_pattern(dico_z, dico_index_x, *x), axis=1
                )

                Retour_omnibus_bypattern = {}

                # Apply the statistic computation by missing patterns
                for pattern in frequent_pattern:
                    try:
                        Retour_omnibus_bypattern[pattern] = stat_compute(
                            np.array(dico_z[pattern]), pattern, dico_index_y[pattern]
                        )
                    except ValueError:
                        print("worktable")

                Retour_omnibus = [0.0 for i in range(sum_stat_tab.shape[0])]

                for pattern in frequent_pattern:
                    for ligne, indice in enumerate(dico_index_x[pattern]):
                        Retour_omnibus[int(indice)] = (
                            Retour_omnibus_bypattern[pattern]
                        )[int(ligne)]

                sum_stat_tab["JASS_PVAL"] = Retour_omnibus

        Nsnp_jassed = Nsnp_jassed + sum_stat_tab.shape[0]
        sum_stat_tab.sort_values(by=["Region", "CHR"], inplace=True)

        sum_stat_tab["UNIVARIATE_MIN_PVAL"] = DataFrame(
            2.0
            * spst.norm.sf(sum_stat_tab[phenotype_ids].fillna(0, inplace=False).abs()),
            index=sum_stat_tab.index,
        ).min(axis=1)

        sum_stat_tab["UNIVARIATE_MIN_QVAL"] = sum_stat_tab["UNIVARIATE_MIN_PVAL"] * (
            1 - np.isnan(sum_stat_tab[phenotype_ids]).astype(int)
        ).sum(1)
        sum_stat_tab.loc[
            sum_stat_tab.UNIVARIATE_MIN_QVAL > 1, "UNIVARIATE_MIN_QVAL"
        ] = 1

        # Computing pleiotropy
        sum_stat_tab["PLEIOTROPY_INDEX"] = compute_pleiotropy_index(
            sum_stat_tab[phenotype_ids], significance_treshold
        )

        sum_stat_tab = sum_stat_tab[
            [
                "Region",
                "CHR",
                "snp_ids",
                "position",
                "Ref_allele",
                "Alt_allele",
                "MiddlePosition",
                "JASS_PVAL",
                "UNIVARIATE_MIN_PVAL",
                "UNIVARIATE_MIN_QVAL",
                "PLEIOTROPY_INDEX",
            ]
            + phenotype_ids
        ]

        if post_filtering:
            sum_stat_tab = post_computation_filtering(sum_stat_tab)

        hdf_work.append(
            "SumStatTab", sum_stat_tab, min_itemsize=sum_stat_tab_min_itemsizes
        )

        if (csv_file is not None) and (not delayed_gen_csv_file):
            with open(csv_file, "a") as f:
                sum_stat_tab.to_csv(f, header=f.tell() == 0)

        region_sub_table = get_region_summary(
            sum_stat_tab, phenotype_ids, significance_treshold=significance_treshold
        )

        hdf_work.append(
            "Regions", region_sub_table, min_itemsize=region_sub_table_min_itemsizes
        )

        if callback_report:
            print("CALLING PROGRESS FUNCTION")
            # the chunk index starts at zero and we take into account the 2 plot stages
            JASS_progress = round((chunk + 1) * 100 / (Nchunk + 2))
            callback_report(int(JASS_progress))

    hdf_work.close()

    print("{1} SNPs treated on {0} SNPs".format(Nsnp_jassed, Nsnp_total))

    RegionSubTable = read_hdf(project_hdf_path, "Regions")

    pval_min = RegionSubTable["UNIVARIATE_MIN_PVAL"]
    jost_min = RegionSubTable["JASS_PVAL"]
    summaryTable = DataFrame(
        np.array(
            [
                [
                    sum(
                        (jost_min < significance_treshold)
                        & (pval_min < significance_treshold)
                    ),
                    sum(
                        (jost_min < significance_treshold)
                        & (pval_min > significance_treshold)
                    ),
                ],
                [
                    sum(
                        (jost_min > significance_treshold)
                        & (pval_min < significance_treshold)
                    ),
                    sum(
                        (jost_min > significance_treshold)
                        & (pval_min > significance_treshold)
                    ),
                ],
            ]
        )
    )
    summaryTable.columns = ["MinUnivSignif", "MinUnivNotSignif"]
    summaryTable.index = ["JASSSignif", "JASSNotSignif"]

    hdf_work = HDFStore(project_hdf_path)
    hdf_work.put(
        "summaryTable", summaryTable, format="table", data_columns=True
    )  # Summary Table (contigency table)
    hdf_work.close()

    return Nchunk


def binary_code(*args):
    """
    binary_code
    Generates the binary code of each pattern ensuring compatibility between Linux and Windows
    """
    Chaine = ""
    for valeur in args:
        Chaine += "{}".format(valeur)
    return int(Chaine, 2)


def binary_code_Big(dico_index_y, *args):
    """
    binary_code
    Generates the binary code of each pattern ensuring compatibility between Linux and Windows
    """
    Chaine = ""
    for valeur in args:
        Chaine += "{}".format(valeur)

    Codage = int(Chaine, 2)

    if not (Codage in dico_index_y):
        dico_index_y[Codage] = []
        for indice, valeur in enumerate(args):
            if valeur == 1:
                dico_index_y[Codage].append(indice)

    return Codage


def store_pattern(dico_z, dico_index_x, *colonne):
    """
    store_pattern
    Reorders z-values by pattern and store their index in the original dataframe
    """
    Index = int(colonne[0])
    Codage = int(colonne[1])

    if not (Codage in dico_z):
        dico_z[Codage] = []
        dico_index_x[Codage] = []

    dico_index_x[Codage].append(Index)

    new_line = []
    for valeur in colonne[2:]:
        if not math.isnan(valeur):
            new_line.append(valeur)

    dico_z[Codage].append(new_line)


def compute_frequent_missing_pattern(sum_stat_tab):
    """
    Compute the frequency of missing pattern in the dataset
    """
    Pheno_is_present = 1 - sum_stat_tab.isnull()

    # The coding of patterns missing is not guaranteed if there are more than 64 phenotypes
    patterns_missing = Pheno_is_present[Pheno_is_present.columns].apply(
        lambda x: binary_code(*x), axis=1
    )

    pattern_frequency = patterns_missing.value_counts() / len(patterns_missing)
    n_pattern = pattern_frequency.shape[0]
    print("Number of pattern {}".format(n_pattern))
    frequent_pattern = pattern_frequency.index.tolist()
    return patterns_missing, frequent_pattern


def compute_frequent_missing_pattern_Big(sum_stat_tab):
    """
    Compute the frequency of missing pattern in the dataset

    """
    dico_index_y = {}

    Pheno_is_present = 1 - sum_stat_tab.isnull()

    # The coding of patterns missing is not guaranteed if there are more than 64 phenotypes
    patterns_missing = Pheno_is_present[Pheno_is_present.columns].apply(
        lambda x: binary_code_Big(dico_index_y, *x), axis=1
    )

    pattern_frequency = patterns_missing.value_counts() / len(patterns_missing)
    n_pattern = pattern_frequency.shape[0]
    print("Number of pattern {}".format(n_pattern))
    frequent_pattern = pattern_frequency.index.tolist()

    return patterns_missing, frequent_pattern, dico_index_y


def stringize_dataframe_region_chr(dataframe: DataFrame):
    """
    Reformat Region and Chromosome numbers as strings in a DataFrame.
    This will eventually be deprecated once the js code accepts integers instead
    of strings

    :param project_hdf_path: path to the worktable file
    :type project_hdf_path: str
    :param frame: name of the frame to be read
    :type frame: str
    :return: The dataframe with converted Region and CHR columns
    :rtype: pandas.DataFrame
    """
    dataframe["Region"] = dataframe["Region"].apply(lambda x: "Region" + str(x))
    dataframe["CHR"] = dataframe["CHR"].apply(lambda x: "chr" + str(x))
    dataframe["JASS_PVAL"] = dataframe["JASS_PVAL"].apply(lambda x: str(signif(x, 4)))

    return dataframe


def get_worktable_phenolist(project_hdf_path: str):
    """
    Read and return the PhenoList dataframe from a worktable file

    :param project_hdf_path: path to the worktable file
    :type project_hdf_path: str
    :return: The dataframe as a dictionary with one entry per line number
    :rtype: dict
    """
    phenolist_dataframe = read_hdf(project_hdf_path, "PhenoList")
    return phenolist_dataframe.to_dict(orient='index')


def get_worktable_summary(project_hdf_path: str):
    """
    Read and return the summaryTable dataframe from a worktable file

    :param project_hdf_path: path to the worktable file
    :type project_hdf_path: str
    :return: The dataframe as a dictionary with one entry per line number
    :rtype: dict
    """
    summary_dataframe = read_hdf(project_hdf_path, "summaryTable")
    lines = {}
    for index, row in summary_dataframe.iterrows():
        lines[index] = {
            "MinUnivSignif": int(row["MinUnivSignif"]),
            "MinUnivNotSignif": int(row["MinUnivNotSignif"]),
        }
    return lines


def get_worktable_gencov(project_hdf_path: str):
    """
    Read and return the summaryTable dataframe from a worktable file

    :param project_hdf_path: path to the worktable file
    :type project_hdf_path: str
    :return: The dataframe as a dictionary with one entry per line number
    :rtype: dict
    """
    phenolist_dataframe = read_hdf(project_hdf_path, "PhenoList")

    gencov = read_hdf(project_hdf_path, "GEN_COV")
    sorted_trait = phenolist_dataframe.Outcome.sort_values()
    gencov = gencov.loc[sorted_trait.index, sorted_trait.index]


    print(sorted_trait)
    lines = []

    for i in range(gencov.shape[0]):
        for j in range(i, gencov.shape[0]):
            if np.isnan(gencov.loc[sorted_trait.index[i], sorted_trait.index[j]]):
                lines.append({
                    "phenotypeID_A": sorted_trait.iloc[i],
                    "phenotypeID_B": sorted_trait.iloc[j],
                    "gencov":"null"
                    })
            else:
                lines.append({
                    "phenotypeID_A": sorted_trait.iloc[i],
                    "phenotypeID_B": sorted_trait.iloc[j],
                    "gencov":gencov.loc[sorted_trait.index[i], sorted_trait.index[j]]
                    })

    print(lines)
    return lines



def get_worktable_genomedata(project_hdf_path: str):
    """
    Read and return the Regions dataframe from a worktable file

    :param project_hdf_path: path to the worktable file
    :type project_hdf_path: str
    :return: The dataframe as a CSV formatted text
    :rtype: str
    """
    region_subtable = stringize_dataframe_region_chr(
        read_hdf(project_hdf_path, "Regions")
    )

    region_subtable.rename(index=str, columns={"JASS_PVAL": "JOSTmin"}, inplace=True)

    region_subtable["PVALmin"] = region_subtable["UNIVARIATE_MIN_PVAL"]
    region_subtable["PVALmin"] = region_subtable["PVALmin"].apply(
        lambda x: str(signif(x, 4))
    )

    return region_subtable.to_csv(index=False)


def get_worktable_local_manhattan_data(
    project_hdf_path: str, chromosome: str = None, region: str = None
):
    """
    Read and return the SumStatTab dataframe from a worktable file
    for a given chromosome and region for the Manhattan plot

    :param project_hdf_path: path to the worktable file
    :type project_hdf_path: str
    :param chromosome: chromosome number in "string form", e.g. chr12
    :type chromosome: str
    :param region: region number in "string form", e.g. Region213
    :type region: str
    :return: The dataframe subset corresponding to the chromosome and region, as a CSV formatted text
    :rtype: str
    """
    if (chromosome is None) and (region is None):
        # Local analysis : the file project_hdf_path contains only useful information.
        # No data filter is needed
        dataframe = read_hdf(
            project_hdf_path,
            "SumStatTab",
            columns=["Region", "CHR", "position", "snp_ids", "JASS_PVAL"],
        )
    else:
        # Genome full analysis
        region_int = region[6:]
        chromosome_int = chromosome[3:]
        dataframe = read_hdf(
            project_hdf_path,
            "SumStatTab",
            columns=["Region", "CHR", "position", "snp_ids", "JASS_PVAL"],
            where=["Region=" + str(region_int), "CHR=" + str(chromosome_int)],
        )

    dataframe = stringize_dataframe_region_chr(dataframe)
    dataframe = dataframe.sort_values("position")

    return dataframe.to_csv(index=False)


def get_worktable_local_heatmap_data(
    project_hdf_path: str, chromosome: str = None, region: str = None
):
    """
    Read and return the SumStatTab dataframe from a worktable file
    for a given chromosome and region for the Heatmap plot

    :param project_hdf_path: path to the worktable file
    :type project_hdf_path: str
    :param chromosome: chromosome number in "string form", e.g. chr12
    :type chromosome: str
    :param region: region number in "string form", e.g. Region213
    :type region: str
    :return: The dataframe subset corresponding to the chromosome and region, \
             pivoted and as a CSV formatted text
    :rtype: str
    """
    if (chromosome is None) and (region is None):
        # Local analysis : the file project_hdf_path contains only useful information.
        # No data filter is needed
        dataframe = read_hdf(project_hdf_path, "SumStatTab")
    else:
        # Genome full analysis
        region_int = region[6:]
        chromosome_int = chromosome[3:]
        dataframe = read_hdf(
            project_hdf_path,
            "SumStatTab",
            where=["Region=" + str(region_int), "CHR=" + str(chromosome_int)],
        )

    dataframe = stringize_dataframe_region_chr(dataframe)
    dataframe = dataframe.sort_values("position")
    dataframe.drop(
        [
            "Region",
            "CHR",
            "Ref_allele",
            "Alt_allele",
            "position",
            "JASS_PVAL",
            "MiddlePosition",
            "UNIVARIATE_MIN_PVAL",
            "UNIVARIATE_MIN_QVAL",
            "PLEIOTROPY_INDEX",
        ],
        axis=1,
        inplace=True,
    )
    dataframe.rename(columns={"snp_ids": "ID"}, inplace=True)
    column_order = list(dataframe.ID)
    pivoted_dataframe = dataframe.pivot_table(columns="ID")
    pivoted_dataframe = pivoted_dataframe.reindex(column_order, axis=1)
    # TODO rework the selection to return 5000 snps in total, around the
    # region ;)

    return pivoted_dataframe.to_csv(index_label="ID")


def create_genome_full_csv(project_hdf_path, csv_file, chunk_size=50, Nchunk=35):
    """
    create_genome_full_csv
    Write the genome_full.csv file

    :param project_hdf_path: path to the worktable file
    :type project_hdf_path: str

    :param csv_file: path to the genome_full file
    :type csv_file: str

    :param chunk_size: the size of the chunks of the initial data
    :type chunk_size: int

    :return: csv_file is available.
    :rtype: bool
    """

    # path of the lock that indicates that the csv file is not available
    the_lock_path = get_genome_full_csv_lock_path(project_hdf_path)
    if os.path.isfile(the_lock_path):
        # The lock is set on
        if os.path.isfile(csv_file):
            # An error occurred: the csv file must not exist if the lock is set
            # The existing csv file is deleted
            os.remove(csv_file)

        for chunk in range(Nchunk):
            print("indice de boucle = {}/{}".format(chunk, Nchunk-1))
            binf = chunk * chunk_size
            bsup = (chunk + 1) * chunk_size

            # read workTable.hdf5
            df_for_csv = read_hdf(
                project_hdf_path,
                "SumStatTab",
                where="Region >= {0} and Region < {1}".format(binf, bsup),
            )

            # append the data to the csv file
            with open(csv_file, "a") as f:
                df_for_csv.to_csv(f, header=f.tell() == 0)

        # The lock is deleted
        os.remove(the_lock_path)

    if os.path.isfile(csv_file):
        The_file_is_available = True
    else:
        The_file_is_available = False

    return The_file_is_available


def get_genome_full_csv_lock_path(project_hdf_path):
    the_lock_path = os.path.join(os.path.dirname(project_hdf_path), "the_lock.txt")
    return the_lock_path


def create_genome_full_csv_lock_file(project_hdf_path):
    # setting a lock to generate the csv_file asynchronously
    the_lock_path = get_genome_full_csv_lock_path(project_hdf_path)
    the_lock = "The lock is set on : workTable.csv is not yet available"
    file_lock = open(the_lock_path, "w")
    file_lock.write(the_lock)
    file_lock.close()
