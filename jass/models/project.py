# -*- coding: utf-8 -*-
"""
compute joint statistics and generate plots for a given set of phenotypes
"""
from __future__ import absolute_import
import abc
import shutil
from datetime import datetime
from json import JSONDecodeError
from pathlib import Path
from celery import states
import json
from typing import List, Optional, Union, Dict
try:
    from typing import Literal
except ImportError:
    from typing_extensions import Literal
import os, sys
import hashlib
from enum import Enum
import traceback


from pydantic import BaseModel, parse_raw_as

from jass.models.phenotype import Phenotype
from jass.models.plots import (
    create_global_plot,
    create_local_plot,
    create_quadrant_plot,
    create_qq_plot,
)
from jass.models.inittable import get_inittable_meta
from jass.models.worktable import (
    get_worktable_summary,
    get_worktable_gencov,
    get_worktable_genomedata,
    get_worktable_local_manhattan_data,
    get_worktable_local_heatmap_data,
    get_worktable_phenolist,
    create_worktable_file,
    create_genome_full_csv,
    create_genome_full_csv_lock_file,
)

from jass.config import config


def call_with_tb(status_key):
    def inner_dec(f):
        def wrapped_method(self, *args, **kwargs):
            self.status[status_key]=ProjectStatusEnum.creating
            self.save()
            try:
                ret = f(self, *args, **kwargs)
                self.status[status_key] = ProjectStatusEnum.ready
                self.save()
                return ret
            except Exception as e:
                self.status[status_key] = ProjectStatusEnum.error
                self.save()
                exc_type, exc_value, exc_traceback = sys.exc_info()
                log_path = os.path.join(self.get_folder_path(), f"{status_key}.log")
                log_fh = open(log_path, "w")
                traceback.print_exception(exc_type, exc_value, exc_traceback, file=log_fh)
                log_fh.close()

        return wrapped_method
    return inner_dec


def get_project_update_progress_callback(project_id):
    def __update_progress(progress: int):
        p = load_project(project_id)
        p.update_progress(progress)
        p.save()
    return __update_progress


class ProjectStatusEnum(str, Enum):
    does_not_exist = "DOES_NOT_EXIST"
    creating = "CREATING"
    ready = "READY"
    error = "ERROR"


class Project(BaseModel, abc.ABC):
    id : Optional[str]
    init_table_path: Optional[str] = os.path.join(config["DATA_DIR"], "initTable.hdf5")
    phenotypes: List[Phenotype]
    remove_nan: Optional[bool] = False
    stat_function: Optional[str] = "jass.models.stats:omnibus_stat"
    optim_na: Optional[bool] = True
    chunk_size: Optional[int] = 50
    significance_threshold: Optional[float] = 5 * 10 ** -8
    post_filtering: Optional[bool] = True
    delayed_gen_csv_file: Optional[bool] = True
    status: Optional[Dict[str,ProjectStatusEnum]] = {}
    progress: Optional[int] = 0
    celery_id: Optional[str] = None

    def get_hash(self):
        m = hashlib.md5()
        m.update(str(self.init_table_path).encode("utf-8"))
        m.update(str(self.remove_nan).encode("utf-8"))
        m.update(str(self.stat_function).encode("utf-8"))
        m.update(str(self.optim_na).encode("utf-8"))
        m.update(str(self.chunk_size).encode("utf-8"))
        m.update(str(self.significance_threshold).encode("utf-8"))
        m.update(str(self.post_filtering).encode("utf-8"))
        m.update(str(self.delayed_gen_csv_file).encode("utf-8"))
        for phenotype in self.phenotypes:
            m.update(str(phenotype.id).encode("utf-8"))
        return m

    def create(self, fail_if_exists=True):
        self.id = self.create_id()
        try:
            os.makedirs(self.get_folder_path())
            flag_project_as_visited(self.id)
            self.save()
            return True
        except FileExistsError as e:
            if fail_if_exists:
                raise e
            if os.path.exists(self.get_meta_file_path()):
                try:
                    load_project(self.id)
                    return False
                except JSONDecodeError as e:
                    print(f"Project {self.id} corrupted ({str(e)}). Removing it, before trying to re-create it.")
                    pass
            # The directory exists, but meta.json don't or is corrupted, we clean up the directory, in case of.
            shutil.rmtree(self.get_folder_path())
            return self.create(fail_if_exists=fail_if_exists)

    def save(self):
        with open(self.get_meta_file_path(), "w") as fp:
            fp.write(self.json())

    def create_id(self):
        return self.get_hash().hexdigest()

    def has_computation_going_on(self):
        if self.celery_id is None:
            return False
        from jass.tasks import celery
        celery_task = celery.AsyncResult(self.celery_id)
        print(self.celery_id, celery_task.status)
        if celery_task.status in [states.FAILURE, states.SUCCESS, states.PENDING]:
            # PENDING means "unknown task" for celery
            return False
        return True

    @classmethod
    def get_folder_path_from_id(cls, project_id):
        return os.path.join(config["PROJECTS_DIR"], f"project_{project_id}")

    @classmethod
    def load(cls, id):
        path = os.path.join(Project.get_folder_path_from_id(id), "meta.json")
        with open(path, "r") as fp:
            return cls.parse_raw("".join(fp.readlines()))

    def update_progress(self, progress):
        self.progress = progress
        self.save()
        print(f"PROGRESS for {self.id}: {self.progress}")

    def get_folder_path(self):
        """
        Get the path of the folder where the project data are stored
        """
        return self.get_folder_path_from_id(self.id)

    def get_meta_file_path(self):
        """
        Get the path of the project meta file
        """
        return os.path.join(self.get_folder_path(), "meta.json")

    def get_worktable_path(self):
        """
        Get the path of the worktable hdf file
        """
        return os.path.join(self.get_folder_path(), "workTable.hdf5")

    def get_csv_path(self):
        """
        Get the path of the full genome CSV file
        """
        return os.path.join(self.get_folder_path(), "workTable.csv")

    def get_global_manhattan_plot_path(self):
        """
        Get the path of the global manhattan plot PNG file
        """
        return os.path.join(self.get_folder_path(), "Manhattan_Plot_Omnibus.png")

    def get_quadrant_plot_path(self):
        """
        Get the path of the quadrant plot PNG file
        """
        return os.path.join(self.get_folder_path(), "Quadrant_Plot_Omnibus.png")

    def get_qq_plot_path(self):
        """
        Get the path of the quadrant plot PNG file
        """
        return os.path.join(self.get_folder_path(), "QQ_Plot_Omnibus.png")

    def get_local_plot_path(self):
        """
        Get the path of the local (zoom) plot PNG file
        """
        return os.path.join(self.get_folder_path(), "Zoom_Plot_Omnibus.png")

    def get_project_summary_statistics(self):
        return get_worktable_summary(self.get_worktable_path())

    def get_metadata_file_path(self):
        """
        Get the path of the metadata json file
        """
        return os.path.join(self.get_folder_path(), "metadata.json")

    def get_project_nsnps(self):
        return get_inittable_meta(self.get_worktable_path())


    def get_project_gencov(self):
        return get_worktable_gencov(self.get_worktable_path())

    def get_project_genomedata(self):
        return get_worktable_genomedata(self.get_worktable_path())

    def get_project_local_manhattan_data(
        self, chromosome: str = None, region: str = None
    ):
        print("project::get_project_local_manhattan_data")
        return get_worktable_local_manhattan_data(
            self.get_worktable_path(), chromosome, region
        )

    def get_project_local_heatmap_data(
        self, chromosome: str = None, region: str = None
    ):
        return get_worktable_local_heatmap_data(
            self.get_worktable_path(), chromosome, region
        )

    @abc.abstractmethod
    def create_worktable_file(self):
        pass

    def delete_large_files(self):
        have_removed_file = False
        # delete the worktable file
        project_hdf_path = self.get_worktable_path()
        if os.path.exists(project_hdf_path):
            have_removed_file = True
            os.remove(project_hdf_path)
        # as a consequence, its creation will have to be redone, so set progress to 0 and remove is status.
        self.progress = 0
        self.status.clear()
        self.save()

        if self.delayed_gen_csv_file:
            # Delete the full csv file if it exists, and re-set the lock file indicating that it should be generated
            create_genome_full_csv_lock_file(project_hdf_path)
            csv_path = self.get_csv_path()
            if os.path.exists(csv_path):
                have_removed_file = True
                os.remove(csv_path)
        for filename in os.listdir(self.get_folder_path()):
            if filename.endswith(".png"):
                os.remove(os.path.join(self.get_folder_path(), filename))
                have_removed_file = True
        try:
            os.remove(self.get_metadata_file_path())
            have_removed_file = True
        except FileNotFoundError:
            pass
        return have_removed_file

    @call_with_tb('global_manhattan')
    def create_global_manhattan_plot(self):
        return create_global_plot(
            self.get_worktable_path(), self.get_global_manhattan_plot_path()
        )

    @call_with_tb('quadrant_plot')
    def create_quadrant_plot(self):
        return create_quadrant_plot(
            self.get_worktable_path(), self.get_quadrant_plot_path()
        )

    @call_with_tb('local_plot')
    def create_local_plot(self):
        return create_local_plot(self.get_worktable_path(), self.get_local_plot_path())

    @call_with_tb('qq_plot')
    def create_qq_plot(self):
        return create_qq_plot(self.get_worktable_path(), self.get_qq_plot_path())

    @call_with_tb('genome_full_csv')
    def create_csv_file(self):
        return create_genome_full_csv(self.get_worktable_path(), self.get_csv_path())

    @call_with_tb('metadata')
    def create_project_metadata_file(self):
        with open(self.get_metadata_file_path(), 'w') as f:
            f.write(json.dumps(self.get_project_nsnps()))
        print("------ metadata -----")


class GlobalProject(Project):
    @call_with_tb('worktable')
    def create_worktable_file(self):
        return create_worktable_file(
            phenotype_ids=[phenotype.id for phenotype in self.phenotypes],
            init_file_path=self.init_table_path,
            project_hdf_path=self.get_worktable_path(),
            remove_nan=self.remove_nan,
            stat=self.stat_function,
            optim_na=self.optim_na,
            csv_file=self.get_csv_path(),
            chunk_size=self.chunk_size,
            significance_treshold=self.significance_threshold,
            post_filtering=self.post_filtering,
            delayed_gen_csv_file=self.delayed_gen_csv_file,
            callback_report=get_project_update_progress_callback(self.id),
        )


# Upper bound of the chromosome length (bp)
K_POS_MAX = 250000000


class LocalProject(Project):
    chromosome: int
    start: int
    end: int = K_POS_MAX

    def get_hash(self):
        m = super().get_hash()
        m.update(self.chromosome)
        m.update(self.start)
        m.update(self.end)
        return m

    @call_with_tb('worktable')
    def create_worktable_file(self):
        return create_worktable_file(
            phenotype_ids=[phenotype.id for phenotype in self.phenotypes],
            init_file_path=self.init_table_path,
            project_hdf_path=self.get_worktable_path(),
            remove_nan=self.remove_nan,
            stat=self.stat_function,
            optim_na=self.optim_na,
            csv_file=self.get_csv_path(),
            chunk_size=self.chunk_size,
            significance_treshold=self.significance_threshold,
            post_filtering=self.post_filtering,
            delayed_gen_csv_file=self.delayed_gen_csv_file,
            chromosome=self.chromosome,
            pos_Start=self.start,
            pos_End=self.end,
            callback_report=get_project_update_progress_callback(self.id),
        )


def load_project(project_id, flag_as_visited: bool = True):
    path = os.path.join(Project.get_folder_path_from_id(project_id), "meta.json")
    with open(path, "r") as fp:
        project = parse_raw_as(
            Union[
                LocalProject,
                GlobalProject,
            ],
            "".join(fp.readlines()),
        )
        if flag_as_visited:
            flag_project_as_visited(project_id)
        return project


def flag_project_as_visited(project_id):
    Path(os.path.join(Project.get_folder_path_from_id(project_id), "last_visit.flag")).touch()


def get_projects_last_access():
    res = []
    for path in Path(config["PROJECTS_DIR"]).iterdir():
        if path.is_file():
            continue
        if not path.name.startswith("project_"):
            continue
        project_id = path.name[8:]
        try:
            last_access = datetime.fromtimestamp(
                os.path.getmtime(os.path.join(Project.get_folder_path_from_id(project_id), "last_visit.flag")))
        except FileNotFoundError:
            flag_project_as_visited(project_id)
            last_access = datetime.now()
        res.append(
            dict(
                project_id=project_id,
                last_access=last_access,
                path=path,
            )
        )
    return res


def ensure_space_in_project_dir(*, except_project_id=None):
    if config["MIN_SIZE_TO_KEEP_IN_PROJECTS_DIR_IN_MB"] <= 0:
        return
    projects = get_projects_last_access()
    if except_project_id is not None:
        projects = filter(lambda p: p['project_id'] != except_project_id, projects)
    projects = sorted(projects, key=lambda p: p['last_access'])
    show_space_message = True
    for proj in projects:
        _, _, free = shutil.disk_usage(config["PROJECTS_DIR"])
        free /= 2 ** 20
        if free > config["MIN_SIZE_TO_KEEP_IN_PROJECTS_DIR_IN_MB"]:
            break
        if show_space_message:
            print(f'Free space is lower than minimum allowed '
                  f'({free} MB<{config["MIN_SIZE_TO_KEEP_IN_PROJECTS_DIR_IN_MB"]} MB), '
                  f'removing data from old projects')
            show_space_message = False
        try:
            if load_project(proj['project_id'], flag_as_visited=False).delete_large_files():
                print(f"Project {proj['project_id']} was last accessed on {proj['last_access']}, data removed.")
        except JSONDecodeError as e:
            print(f"Project {proj['project_id']} has a corrupted meta.json ({str(e)}), removing it.")
            shutil.rmtree(proj['path'])

