# -*- coding: utf-8 -*-
"""
Created on Tue Mar 28 09:57:33 2017

@author: vguillem, hjulienne, hmenager
"""
import os
import re
import glob
import logging
from pandas import HDFStore, DataFrame, read_csv, concat, options, read_hdf
import numpy as np
import tables
import warnings
from functools import reduce

options.mode.chained_assignment = None
warnings.filterwarnings("ignore", category=tables.NaturalNameWarning)

valideInitTableNamePattern = re.compile("^([a-zA-Z0-9_-]+).hdf5$") #re.compile("^([A-Z]*[a-z]*-*\.?[0-9]*)+$")

class InitMeta(object):
    def __init__(self, nb_snps, nb_phenotypes):
        self.nb_snps = nb_snps
        self.nb_phenotypes = nb_phenotypes

def get_inittable_meta(file_name):
    init_store = HDFStore(file_name, mode='r')
    nb_snps = init_store.get_storer("SumStatTab").nrows
    metadata = dict(
        title=f"Filename: {file_name.split('/')[-1]}",
        description="No description",
        ancestry="??",
        assembly="????",
    )
    try:
        df = init_store.get('METADATA')
        for i in range(len(df)):
            metadata[df.iloc[i, 0]] = df.iloc[i, 1]
    except KeyError:
        pass
    init_store.close()
    nb_phenotypes = read_hdf(file_name, "PhenoList").shape[0]

    return dict(
        nb_snps=int(nb_snps),
        nb_phenotypes=int(nb_phenotypes),
        name=metadata['title'],
        desc=metadata['description'],
        **dict(
            (k, metadata[k])
            for k in set(metadata.keys()) if k not in {
                'title',
                'description',
            }
        ),
    )


def get_gwasname(file_name):
    return "_".join(os.path.basename(file_name).split("_")[0:3])

def check_if_SNP_unique(z_gwas_chrom):
    tg = z_gwas_chrom.groupby(["snp_ids"]).count()["CHR"].reset_index()
    if len(tg.loc[tg["CHR"] > 1, "CHR"]) > 0:
        msg = "Bad input : SNP {0} have several positions or reference allele".format(
            " ".join(tg.loc[tg["CHR"] > 0, "snp_ids"])
        )
        raise IOError(msg)

def get_gwas_dict(input_data_path):
    gwas_dict = {}
    # retrieve all files corresponding to glob patterns
    files_by_pattern = list(map(glob.glob, input_data_path.split(";")))
    all_path = reduce(lambda x, y: x + y, files_by_pattern)
    print(all_path)
    for x in all_path:
        gwas_name = get_gwasname(x)

        if gwas_name in gwas_dict:
            gwas_dict[gwas_name].append(x)
        else:
            gwas_dict[gwas_name] = [x]
    return gwas_dict


def create_pheno_summary(description):
    pheno_summary_data = description[
        [
            "Consortium",
            "Outcome",
            "FullName",
            "Type",
            "Reference",
            "ReferenceLink",
            "dataLink",
            "internalDataLink",
            "Nsample",
            "Ncase",
            "Ncontrol",
        ]
    ]

    pheno_summary_data["ID"] = (
        "z_"
        + pheno_summary_data.Consortium.str.upper()
        + "_"
        + pheno_summary_data.Outcome.str.upper()
    )

    is_quantitatif = pheno_summary_data["Ncase"].isnull()
    pheno_summary_data["Effective_sample_size"] = np.nan
    pheno_summary_data.loc[~is_quantitatif, "Effective_sample_size"] = (
        pheno_summary_data.loc[~is_quantitatif, "Ncase"]
        * pheno_summary_data.loc[~is_quantitatif, "Ncontrol"]
    ) / pheno_summary_data.loc[~is_quantitatif, "Nsample"]

    pheno_summary_data.loc[
        is_quantitatif, "Effective_sample_size"
    ] = pheno_summary_data.loc[is_quantitatif, "Nsample"]

    # reorder columns in the dataframe
    pheno_summary_data = pheno_summary_data[
        [
            "ID",
            "Consortium",
            "Outcome",
            "FullName",
            "Type",
            "Reference",
            "ReferenceLink",
            "dataLink",
            "internalDataLink",
            "Nsample",
            "Ncase",
            "Ncontrol",
            "Effective_sample_size",
        ]
    ]
    pheno_summary_data.index = pheno_summary_data["ID"]
    return pheno_summary_data


def format_chr_gwas(gwas_file_chri, chrom, study_name, regions_bychr):

    z_gwas = read_csv(
        gwas_file_chri, sep="\t", usecols=[0, 1, 2, 3, 4], memory_map=True
    )

    z_gwas.columns = [
        "snp_ids",
        "position",
        "Ref_allele",
        "Alt_allele",
        "z_score",
    ]

    # CAUTION: handle duplicates (whenever there are multiple values
    # for a single SNP)
    z_gwas.drop_duplicates(subset=["snp_ids"], keep="first", inplace=True)

    # Add chromosome and study name columns
    z_gwas["CHR"] = chrom
    z_gwas["study_name"] = study_name

    # Add region and middle position columns
    z_gwas["Region"] = 0
    z_gwas["MiddlePosition"] = 0.0

    for region_index, region_row in regions_bychr.get_group("chr%d" % chrom).iterrows():
        left = region_row["start"]
        right = region_row["stop"]
        ind = (z_gwas["position"] >= left) & (z_gwas["position"] <= right)
        (z_gwas.loc[ind, "Region"]) = np.int64(region_index + 1)
        (z_gwas.loc[ind, "MiddlePosition"]) = (left + right) / 2
    return z_gwas


def compute_covariance_zscore(init_file_path, path_covariance=None):
    print("## Compute covariance ##")
    sum_stat_jost_tab = read_hdf(
        init_file_path,
        "SumStatTab",
        where="Region >= {0} and Region < {1}".format(0, 3),
    )
    trait = [i for i in sum_stat_jost_tab.columns if i[:2] == "z_"]

    NSNP_matrix = DataFrame(index=trait, columns=trait)
    cov_matrix = DataFrame(index=trait, columns=trait)

    cov_matrix.fillna(0.0, inplace=True)
    NSNP_matrix.fillna(0.0, inplace=True)
    Regions = read_hdf(
        init_file_path,
        "Regions")
    bi = range(0, Regions.shape[0], 50)
    n_len = len(bi) - 1

    for i in range(n_len):
        binf = bi[i]
        bsup = bi[(i + 1)]
        sum_stat_jost_tab = read_hdf(
            init_file_path,
            "SumStatTab",
            where="Region >= {0} and Region < {1}".format(binf, bsup),
        )

        print("Regions {0} to {1}\r".format(binf, bsup))
        j = 0
        for tr1 in trait:
            for tr2 in trait[j:]:
                sum_stat_jost_tab[[tr1, tr2]].replace([np.inf, -np.inf], np.nan, inplace=True)
                cc = sum_stat_jost_tab[[tr1, tr2]].dropna()
                cc = cc.loc[cc.max(1) < 4]

                cov_matrix.loc[tr1, tr2] += cc.iloc[:, 0].dot(cc.iloc[:, 1])
                NSNP_matrix.loc[tr1, tr2] += cc.shape[0]

                cov_matrix.loc[tr2, tr1] += cc.iloc[:, 0].dot(cc.iloc[:, 1])
                NSNP_matrix.loc[tr2, tr1] += cc.shape[0]
            j = j + 1
        print(cov_matrix/NSNP_matrix)
    if path_covariance:
        (cov_matrix/NSNP_matrix).to_csv(path_covariance, sep="\t")
    hdf_init = HDFStore(init_file_path)
    hdf_init.put("COV", (cov_matrix / NSNP_matrix), format="table", data_columns=True)
    hdf_init.close()



def create_inittable_file(
    input_data_path: str,
    regions_map_path: str,
    description_file_path: str,
    init_table_path: str,
    init_covariance_path=None,
    init_genetic_covariance_path=None,
    init_table_metadata_path=None,
):
    # Read region file
    regions = read_csv(regions_map_path, sep="\s+", memory_map=True)
    # Create HDFStore
    if os.path.exists(init_table_path):
        os.remove(init_table_path)
    hdf_init = HDFStore(init_table_path)

    # Read covariance file
    if init_covariance_path != None:
        covariance = read_csv(
            init_covariance_path, sep="\t", index_col=0, memory_map=True
        )
        compute_covariance = False
    else:
        compute_covariance = True

    # Read description file
    description = read_csv(description_file_path, sep="\t")
    gwas_dict = get_gwas_dict(input_data_path)
    print(gwas_dict)
    phenotypes_list = []

    for meta_index, meta_row in description.iterrows():
        # GWAS loop
        # create an ID from the GWAS consortia + outcome columns
        phenotypes_list.append(
            "z_" + meta_row["Consortium"].upper() + "_" + meta_row["Outcome"].upper()
        )

    pheno_select = list(list(phenotypes_list) & gwas_dict.keys())  # &covariance.columns
    pheno_summary_data = create_pheno_summary(description)

    # select only phenotypes for which there is a covariance
    pheno_list = pheno_summary_data.loc[pheno_select, :]
    if compute_covariance == False:
        COV = covariance.loc[pheno_select, pheno_select]
        hdf_init.put("COV", COV, format="table", data_columns=True)

    # Read Genetic Covariance file and add genetic correlation if available
    if init_genetic_covariance_path != None:
        genetic_covariance = read_csv(
            init_genetic_covariance_path, sep="\t", index_col=0, memory_map=True
        )
        GEN_COV = genetic_covariance.loc[pheno_select, pheno_select]
        hdf_init.put("GEN_COV", GEN_COV, format="table", data_columns=True)

    # Read metadata from file and store it
    if init_table_metadata_path is not None:
        metadata = read_csv(init_table_metadata_path, sep='\t', quotechar='"', index_col=False, memory_map=True)
        hdf_init.put("METADATA", metadata, format="table", data_columns=True)

    which_cols = [
        "Region",
        "CHR",
        "MiddlePosition",
        "position",
        "snp_ids",
        "Ref_allele",
        "Alt_allele",
    ]

    which_cols.extend(list(pheno_select))
    hdf_init.put("PhenoList", pheno_list, format="table", data_columns=True)
    hdf_init.put("Regions", regions, format="table", data_columns=True)
    sum_stat_tab_min_itemsizes = {"snp_ids": 310, "Ref_allele": 300, "Alt_allele": 300}

    regions_bychr = regions.groupby("chr")

    for chrom in range(1, 23):  # Chromosome loop
        print(chrom)
        z_gwas_long_chrom = DataFrame()
        logging.debug("Processing chromosome %d" % chrom)
        for meta_index, meta_row in description.iterrows():  # GWAS loop
            # create an ID from the GWAS consortia + outcome columns
            study_name = (
                "z_"
                + meta_row["Consortium"].upper()
                + "_"
                + meta_row["Outcome"].upper()
            )

            print(study_name)
            logging.debug("Processing study %s" % study_name)
            logging.info("Processing study %s" % study_name)
            try:
                gwas_file_chri = os.path.join(
                    os.path.dirname(input_data_path),
                    study_name + "_chr" + str(chrom) + ".txt",
                )
                if gwas_file_chri not in gwas_dict[study_name]:
                    logging.warning(
                        "file %s missing for chromosome no.%s / GWAS %s"
                        % (gwas_file_chri, chrom, study_name)
                    )
                    continue
            except KeyError:
                logging.warning("files missing for GWAS %s" % (study_name))
                # As it is the pipeline can't handle missing files or empty files
                continue

            # Load summary statistics file for CHR i
            z_gwas = format_chr_gwas(gwas_file_chri, chrom, study_name, regions_bychr)
            z_gwas_long_chrom = concat(
                [
                    z_gwas_long_chrom,
                    z_gwas[
                        [
                            "snp_ids",
                            "position",
                            "Ref_allele",
                            "Alt_allele",
                            "Region",
                            "MiddlePosition",
                            "CHR",
                            "study_name",
                            "z_score",
                        ]
                    ],
                ],
                axis=0,
            )

        if len(z_gwas_long_chrom) > 0:
            z_gwas_chrom = z_gwas_long_chrom.pivot_table(
                index=[
                    "snp_ids",
                    "position",
                    "Ref_allele",
                    "Alt_allele",
                    "CHR",
                    "Region",
                    "MiddlePosition",
                ],
                columns="study_name",
                values="z_score",
            )
            z_gwas_chrom.reset_index(inplace=True)

            # Check if only one Reference_allele and position by SNP
            check_if_SNP_unique(z_gwas_chrom)

            sum_stat_tab = z_gwas_chrom[which_cols]

            hdf_init.append(
                "SumStatTab",
                sum_stat_tab,
                min_itemsize=sum_stat_tab_min_itemsizes,
                data_columns=["Region"],
            )
    hdf_init.close()

    if compute_covariance:
        print("Compute Covariance from Zscores")
        compute_covariance_zscore(init_table_path)


def add_gene_annotation(
    gene_data_path, initTable_path=None, df_gene_csv_path=None, df_exon_csv_path=None
):
    """
    add_gene_annotation
    for the first 22 chromosomes, retrieves the label of the genes
    and their position as well as those of the exons associated with the genes.
    Then store this information in a hdf5 file

    :param gene_data_path: path to the GFF file containing gene and exon data (for example, GRCh37_latest_genomic.gff)
    :type gene_data_path: str
    :param initTable_path: path to the file initTable.hdf5
    :type initTable_path: str
    :param df_gene_csv_path: path to the file df_gene.csv
    :type df_gene_csv_path: str
    :param df_exon_csv_path: path to the file df_exon.csv
    :type df_exon_csv_path: str

    :return: the dataframes df_gene and df_exon
    :rtype: 2 PANDAS dataframes
    """
    # lists containing gene data
    gene_id_label = []
    gene_GeneID = []
    gene_chr = []
    gene_start = []
    gene_end = []
    gene_direction = []

    # lists containing exon data
    exon_id_label = []
    exon_GeneID = []
    exon_chr = []
    exon_start = []
    exon_end = []
    exon_direction = []

    # temporary list containing the data of all exons
    TMP__exon_id_label = []
    TMP__exon_GeneID = []
    TMP__exon_chr = []
    TMP__exon_start = []
    TMP__exon_end = []
    TMP__exon_direction = []

    fichier = open(gene_data_path, "r")
    lignes = fichier.readlines()
    fichier.close()

    for ligne in lignes:
        elements = ligne.split("\t")
        if elements[0].startswith("NC_"):
            decode_chr = elements[0].strip("NC_").split(".")
            chr = int(decode_chr[0])
            if chr <= 22:
                if elements[2] == "gene":
                    gene_chr.append(chr)
                    gene_start.append(int(elements[3]))
                    gene_end.append(int(elements[4]))
                    gene_direction.append(elements[6])
                    decode_id_1 = elements[8].split(";")
                    decode_id_2 = decode_id_1[0].split("=")
                    gene_id_label.append(decode_id_2[1].strip("gene-"))
                    decode_id_3 = decode_id_1[1].split(",")
                    decode_id_4 = decode_id_3[0].split("=")
                    decode_id_5 = decode_id_4[1].split(":")
                    gene_GeneID.append(decode_id_5[1])

                elif elements[2] == "exon":
                    TMP__exon_chr.append(chr)
                    TMP__exon_start.append(int(elements[3]))
                    TMP__exon_end.append(int(elements[4]))
                    TMP__exon_direction.append(elements[6])
                    decode_id_1 = elements[8].split(";")
                    decode_id_2 = decode_id_1[0].split("=")
                    TMP__exon_id_label.append(decode_id_2[1].strip("exon-"))
                    decode_id_3 = decode_id_1[2].split(",")
                    decode_id_4 = decode_id_3[0].split("=")
                    decode_id_5 = decode_id_4[1].split(":")
                    TMP__exon_GeneID.append(decode_id_5[1])

    # We only keep the exons that correspond to a gene
    for i in range(len(TMP__exon_id_label)):
        if TMP__exon_GeneID[i] in gene_GeneID:
            exon_id_label.append(TMP__exon_id_label[i])
            exon_GeneID.append(TMP__exon_GeneID[i])
            exon_chr.append(TMP__exon_chr[i])
            exon_start.append(TMP__exon_start[i])
            exon_end.append(TMP__exon_end[i])
            exon_direction.append(TMP__exon_direction[i])

    # We insert genes and exons into dataframes
    df_gene = DataFrame(
        {
            "Chr": gene_chr,
            "GeneID": gene_GeneID,
            "gene_label": gene_id_label,
            "start": gene_start,
            "end": gene_end,
            "direction": gene_direction,
        }
    )
    df_exon = DataFrame(
        {
            "Chr": exon_chr,
            "exon_label": exon_id_label,
            "GeneID": exon_GeneID,
            "start": exon_start,
            "end": exon_end,
            "direction": exon_direction,
        }
    )

    # The rows of the dataframes are sorted by chromosome number and start position (and end position for exon)
    df_gene.sort_values(by=["Chr", "start"], inplace=True, ignore_index=True)
    df_exon.sort_values(by=["Chr", "start", "end"], inplace=True, ignore_index=True)

    # This information about genes and exons is stored in an hdf5 file if possible
    if initTable_path is not None:
        hdf_file = HDFStore(initTable_path)
        hdf_file.put("Gene", df_gene, format="table", data_columns=True)
        hdf_file.put("Exon", df_exon, format="table", data_columns=True)
        hdf_file.close()

    # Dataframe df_gene is stored in a csv file if possible
    if df_gene_csv_path is not None:
        df_gene.to_csv(df_gene_csv_path)

    # Dataframe df_exon is stored in a csv file if possible
    if df_exon_csv_path is not None:
        df_exon.to_csv(df_exon_csv_path)

    return (df_gene, df_exon)
