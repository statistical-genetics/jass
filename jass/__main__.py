#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import shutil
import sys
import argparse
from datetime import timedelta, datetime, date
from json import JSONDecodeError
import uvicorn

from jass.config import config

from jass.models.phenotype import get_available_phenotypes
from jass.models.inittable import create_inittable_file, add_gene_annotation
from jass.models.worktable import create_worktable_file
from jass.models.project import get_projects_last_access, load_project
from jass.models.plots import (
    create_global_plot,
    create_quadrant_plot,
    create_local_plot,
    create_qq_plot,
)
from jass.models.gain import create_features, compute_gain

from pandas import read_hdf

def absolute_path_of_the_file(fileName, output_file=False):
    """
    Builds the absolute path of the file : fileName
    This makes the execution of JASS functions more robust and flexible
    """

    # Build an absolute path if possible
    absoluteFilePath = os.path.abspath(fileName)

    # Test if the file name is a pattern
    is_a_pattern = os.path.basename(fileName).find("*") > -1

    if is_a_pattern or output_file:
        # Test if the directory path exist
        Directory_path_exist = os.path.exists(os.path.dirname(absoluteFilePath))

        if Directory_path_exist == False:
            # Test the path using the Jass data directory
            absoluteFilePath = os.path.normpath(
                os.path.join(config["DATA_DIR"], fileName)
            )

            Directory_path_exist = os.path.exists(os.path.dirname(absoluteFilePath))

        if Directory_path_exist == False:
            Message = "The directory {} does not exist".format(os.path.dirname(absoluteFilePath))
            raise NameError(Message)
    else:
        # Test if the file path exist
        File_path_exist = os.path.exists(absoluteFilePath)

        if File_path_exist == False:
            # Test the path using the Jass data directory
            absoluteFilePath = os.path.normpath(
                os.path.join(config["DATA_DIR"], fileName)
            )
            File_path_exist = os.path.exists(absoluteFilePath)

        if File_path_exist == False:
            Message = "The file {} does not exist".format(fileName)
            raise NameError(Message)

        # Test if it is realy a file
        Is_a_file = os.path.isfile(absoluteFilePath)

        if not Is_a_file:
            Message = "{} is not a file".format(fileName)
            raise NameError(Message)

    return absoluteFilePath


def serve(args):
    if not os.path.exists('client/dist'):
        print("Application cannot be served unless you build the client.")
        print("To do so, please run :")
        print("    cd client")
        print("    yarn install")
        print("    export BASE_PATH='/webui/'")
        print("    yarn generate")
        exit(1)
    uvicorn.run(app='jass.server:app', host=config["HOST"], port=int(config["PORT"]))


def w_list_phenotypes(args):
    phenotypes = get_available_phenotypes(args.init_table_path)
    print([phenotype.id for phenotype in phenotypes])


def w_extract_tsv(args):
    hdf5_path = args.hdf5_table_path
    csv_path = args.tsv_path
    table_key = args.table_key
    if table_key!="SumStatTab":
        table = read_hdf(hdf5_path, table_key)
        table.to_csv(csv_path, sep="\t")
    else:
        append=0
        regions = read_hdf(hdf5_path, "Regions")
        if 'Region' in regions.columns:
            regions = regions.Region.tolist()
        else:
            regions = regions.index.tolist()
        end_reg = max(regions)
        start_reg = min(regions)
        slice_size = 50
        for binf in range(start_reg, (end_reg+slice_size), slice_size):
            sum_stat_tab = read_hdf(
                hdf5_path,
                "SumStatTab",
                where="Region >= {0} and Region < {1}".format(binf, binf+slice_size)
                )
            if append==0:
                with open(csv_path, "w") as f:
                    sum_stat_tab.to_csv(f, sep="\t")
                append=1
            else:
                with open(csv_path, "a") as f:
                    sum_stat_tab.to_csv(f, header=0, sep="\t")



def compute_worktable(args):

    csv_file_path = args.csv_file_path
    if csv_file_path is not None:
        csv_file_path = absolute_path_of_the_file(csv_file_path, True)
    init_table_path = absolute_path_of_the_file(args.init_table_path)
    worktable_path = absolute_path_of_the_file(args.worktable_path, True)
    selected_phenotypes = args.phenotypes
    remove_nan = args.remove_nans
    significance_treshold = float(args.significance_treshold)
    post_filtering = bool(args.post_filtering)
    custom_loadings = args.custom_loadings
    chromosome = args.chromosome_number
    pos_Start = args.start_position
    pos_End = args.end_position

  
    if args.sumz:
        strategy = "jass.models.stats:sumz_stat"
    elif args.fisher_test:
        strategy = "jass.models.stats:fisher_test"
    elif args.meta_analysis:
        strategy = "jass.models.stats:meta_analysis"
    else:
        strategy = "jass.models.stats:omnibus_stat"
    #elif args.strategy is not None:
    #    strategy = args.strategy

    create_worktable_file(
        phenotype_ids=selected_phenotypes,
        init_file_path=init_table_path,
        project_hdf_path=worktable_path,
        remove_nan=remove_nan,
        stat=strategy,
        optim_na=True,
        csv_file=csv_file_path,
        chunk_size=int(args.chunk_size),
        significance_treshold=significance_treshold,
        post_filtering=post_filtering,
        delayed_gen_csv_file=False,
        chromosome=chromosome,
        pos_Start=pos_Start,
        pos_End=pos_End,
        loadings=custom_loadings,
    )


def w_create_worktable(args):
    compute_worktable(args)


def w_create_project_data(args):
    compute_worktable(args)
    worktable_path = absolute_path_of_the_file(args.worktable_path, True)
    manhattan_plot_path = args.manhattan_plot_path
    if manhattan_plot_path is not None:
        manhattan_plot_path = absolute_path_of_the_file(manhattan_plot_path, True)
        create_global_plot(worktable_path, manhattan_plot_path)
    quadrant_plot_path = args.quadrant_plot_path
    if quadrant_plot_path is not None:
        quadrant_plot_path = absolute_path_of_the_file(quadrant_plot_path, True)
        create_quadrant_plot(
            worktable_path,
            quadrant_plot_path,
            significance_treshold=float(args.significance_treshold),
        )
    zoom_plot_path = args.zoom_plot_path
    if zoom_plot_path is not None:
        zoom_plot_path = absolute_path_of_the_file(zoom_plot_path, True)
        create_local_plot(worktable_path, zoom_plot_path)
    qq_plot_path = args.qq_plot_path
    if qq_plot_path is not None:
        qq_plot_path = absolute_path_of_the_file(qq_plot_path, True)
        create_qq_plot(worktable_path, qq_plot_path)


def w_clean_project_data(args):
    shift = timedelta(days=args.max_days_without_access)
    for proj in get_projects_last_access():
        print(f"Project {proj['project_id']} was last accessed on {proj['last_access']}, ", end='')
        if proj['last_access'] + shift < datetime.now():
            try:
                if load_project(project_id=proj['project_id'], flag_as_visited=False).delete_large_files():
                    print("removing its large files")
                else:
                    print("large files already removed")
            except JSONDecodeError as e:
                print("removing as meta.json is corrupted (" + str(e) + ")")
                shutil.rmtree(proj['path'])
        else:
            print("keeping it")


def w_create_inittable(args):
    input_data_path = absolute_path_of_the_file(args.input_data_path)
    init_covariance_path = args.init_covariance_path
    init_genetic_covariance_path = args.init_genetic_covariance_path

    #if genetic and H0 covariance provided search its absolute path
    if init_covariance_path is not None:
        init_covariance_path = absolute_path_of_the_file(init_covariance_path)

    if init_genetic_covariance_path is not None:
        init_genetic_covariance_path = absolute_path_of_the_file(init_genetic_covariance_path)

    regions_map_path = absolute_path_of_the_file(args.regions_map_path)
    description_file_path = absolute_path_of_the_file(args.description_file_path)
    
    if args.init_table_metadata_path is not None:
        init_table_metadata_path = absolute_path_of_the_file(args.init_table_metadata_path)
    else:
        init_table_metadata_path = None     
    init_table_path = absolute_path_of_the_file(args.init_table_path, True)

    create_inittable_file(
        input_data_path,
        regions_map_path,
        description_file_path,
        init_table_path,
        init_covariance_path,
        init_genetic_covariance_path,
        init_table_metadata_path=init_table_metadata_path,
    )


def w_plot_manhattan(args):
    worktable_path = absolute_path_of_the_file(args.worktable_path)
    plot_path = absolute_path_of_the_file(args.plot_path, True)
    create_global_plot(worktable_path, plot_path)


def w_plot_quadrant(args):
    worktable_path = absolute_path_of_the_file(args.worktable_path)
    plot_path = absolute_path_of_the_file(args.plot_path, True)
    significance_treshold = float(args.significance_treshold)
    create_quadrant_plot(
        worktable_path, plot_path, significance_treshold=significance_treshold
    )

def w_qq_plot(args):
    worktable_path = absolute_path_of_the_file(args.worktable_path)
    plot_path = absolute_path_of_the_file(args.plot_path, True)
    create_qq_plot(
        worktable_path, plot_path
        )

def w_gene_annotation(args):
    gene_data_path = absolute_path_of_the_file(args.gene_data_path)
    initTable_path = absolute_path_of_the_file(args.init_table_path, True)
    df_gene_csv_path = absolute_path_of_the_file(args.gene_csv_path, True)
    df_exon_csv_path = absolute_path_of_the_file(args.exon_csv_path, True)
    add_gene_annotation(
        gene_data_path, initTable_path, df_gene_csv_path, df_exon_csv_path
    )

def w_compute_gain(args):
    inittable_path = absolute_path_of_the_file(args.inittable_path)
    combi_path = absolute_path_of_the_file(args.combination_path)
    combi_path_with_gain = absolute_path_of_the_file(args.gain_path, True)
    
    features = create_features(inittable_path, combi_path)
    compute_gain(features, combi_path_with_gain)



def get_parser():
    parser = argparse.ArgumentParser(prog="jass")
    subparsers = parser.add_subparsers(dest="action")
    subparsers.required = True

    parser_serve = subparsers.add_parser("serve", help="run JASS web server")
    parser_serve.set_defaults(func=serve)

    # ------- list-phenotypes -------

    parser_list_phe = subparsers.add_parser(
        "list-phenotypes", help="list phenotypes available in a data file"
    )
    parser_list_phe.add_argument(
        "--init-table-path",
        default=os.path.join(config["DATA_DIR"], "initTable.hdf5"),
        help="path to the initial data file, default is the configured path (JASS_DATA_DIR/initTable.hdf5)",
    )
    parser_list_phe.set_defaults(func=w_list_phenotypes)

    # ------- create-project-data -------

    parser_create_pd = subparsers.add_parser(
        "create-project-data",
        help="compute joint statistics and generate plots for a given set of phenotypes",
    )
    parser_create_pd.add_argument(
        "--init-table-path",
        default=os.path.join(config["DATA_DIR"], "initTable.hdf5"),
        help="path to the initial data file, default is the configured path (JASS_DATA_DIR/initTable.hdf5)",
    )
    parser_create_pd.add_argument(
        "--phenotypes", nargs="+", required=True, help="list of selected phenotypes"
    )
    parser_create_pd.add_argument(
        "--worktable-path", required=True, help="path to the worktable file to generate"
    )
    parser_create_pd.add_argument("--remove-nans", action="store_true", default=False)
    parser_create_pd.add_argument(
        "--manhattan-plot-path",
        required=False,
        help="path to the genome-wide manhattan plot to generate",
    )
    parser_create_pd.add_argument(
        "--quadrant-plot-path",
        required=False,
        help="path to the quadrant plot to generate",
    )
    parser_create_pd.add_argument(
        "--zoom-plot-path",
        required=False,
        help="path to the local plot to generate",
    )
    parser_create_pd.add_argument(
        "--qq-plot-path",
        required=False,
        help="path to the quantile-quantile plot to generate",
    )
    parser_create_pd.add_argument(
        "--significance-treshold",
        default=5 * 10 ** -8,
        help="The treshold at which a p-value is considered significant",
    )
    parser_create_pd.add_argument(
        "--post-filtering",
        default=True,
        help="If a filtering to remove outlier will be applied (in this case the result of SNPs considered aberant will not appear in the worktable)",
    )

    parser_create_pd.add_argument(
        "--custom-loadings",
        required=False,
        help="path toward a csv file containing custom loading for sumZ tests",
    )

    parser_create_pd.add_argument(
        "--chunk-size",
        required=False,
        default=50,
        help="Number of region to load in memory at once",
    )

    parser_create_pd.add_argument(
        "--csv-file-path", required=False, help="path to the results file in csv format"
    )

    parser_create_pd.add_argument(
        "--chromosome-number",
        required=False,
        help="option used only for local analysis: chromosome number studied",
    )

    parser_create_pd.add_argument(
        "--start-position",
        required=False,
        help="option used only for local analysis: start position of the region studied",
    )

    parser_create_pd.add_argument(
        "--end-position",
        required=False,
        help="option used only for local analysis: end position of the region studied",
    )

    strategies = parser_create_pd.add_mutually_exclusive_group()
    strategies.add_argument("--omnibus", action="store_true", default=True)
    strategies.add_argument("--sumz", action="store_true", default=False)
    strategies.add_argument("--fisher_test", action="store_true", default=False)
    strategies.add_argument("--meta_analysis", action="store_true", default=False)

    parser_create_pd.set_defaults(func=w_create_project_data)

    # ------- clean-project-data -------

    parser_clean_pd = subparsers.add_parser(
        "clean-project-data",
        help="Remove old projects data that haven't been accessed recently",
    )
    parser_clean_pd.add_argument(
        "--max-days-without-access",
        type=int,
        default=30,
        help="A project is marked for large file deletion if the number of days elapsed since "
             "the last access is greater than the amount provided.",
    )
    parser_clean_pd.set_defaults(func=w_clean_project_data)

    # ------- create-inittable -------

    parser_create_it = subparsers.add_parser(
        "create-inittable", help="import data into an initial data file"
    )
    parser_create_it.add_argument(
        "--input-data-path",
        required=True,
        help="path to the GWAS data file (ImpG format) to import. the path must be specify between quotes",
    )
    parser_create_it.add_argument(
        "--init-covariance-path",
        default=None,
        help="path to the covariance file to import",
    )
    parser_create_it.add_argument(
        "--regions-map-path",
        required=True,
        help="path to the genome regions map (BED format) to import",
    )
    parser_create_it.add_argument(
        "--description-file-path",
        required=True,
        help="path to the GWAS studies metadata file",
    )
    parser_create_it.add_argument(
        "--init-table-path",
        default=os.path.join(config["DATA_DIR"], "initTable.hdf5"),
        help="path to the initial data file to produce, default is the configured path (JASS_DATA_DIR/initTable.hdf5)",
    )
    parser_create_it.add_argument(
        "--init-genetic-covariance-path",
        required=False,
        default=None,
        help="path to the genetic covariance file to import. Used only for display on Jass web application",
    )
    parser_create_it.add_argument(
        "--init-table-metadata-path",
        required=False,
        default=None,
        help="path to metadata file to attache to the initial data file",
    )
    
    parser_create_it.set_defaults(func=w_create_inittable)
    # ------- create-worktable -------

    parser_create_wt = subparsers.add_parser(
        "create-worktable",
        help="compute joint statistics for a given set of phenotypes",
    )
    parser_create_wt.add_argument(
        "--init-table-path",
        default=os.path.join(config["DATA_DIR"], "initTable.hdf5"),
        help="path to the initial data table, default is the configured path (JASS_DATA_DIR/initTable.hdf5)",
    )
    parser_create_wt.add_argument(
        "--phenotypes", nargs="+", required=True, help="list of selected phenotypes"
    )
    parser_create_wt.add_argument(
        "--worktable-path", required=True, help="path to the worktable file to generate"
    )
    parser_create_wt.add_argument(
        "--significance-treshold",
        default=5 * 10 ** -8,
        help="threshold at which a p-value is considered significant",
    )
    parser_create_wt.add_argument(
        "--post-filtering",
        default=True,
        help="If a filtering to remove outlier will be applied (in this case the result of SNPs considered aberant will not appear in the worktable)",
    )

    parser_create_wt.add_argument(
        "--custom-loadings",
        required=False,
        help="path toward a csv file containing custom loading for sumZ tests",
    )

    parser_create_wt.add_argument(
        "--csv-file-path", required=False, help="path to the results file in csv format"
    )

    parser_create_wt.add_argument(
        "--chunk-size",
        required=False,
        default=50,
        help="Number of region to load in memory at once",
    )

    parser_create_wt.add_argument("--remove-nans", action="store_true", default=False)

    parser_create_wt.add_argument(
        "--chromosome-number",
        required=False,
        help="option used only for local analysis: chromosome number studied",
    )

    parser_create_wt.add_argument(
        "--start-position",
        required=False,
        help="option used only for local analysis: start position of the region studied",
    )

    parser_create_wt.add_argument(
        "--end-position",
        required=False,
        help="option used only for local analysis: end position of the region studied",
    )

    strategies = parser_create_wt.add_mutually_exclusive_group()
    strategies.add_argument("--omnibus", action="store_true", default=True)
    strategies.add_argument("--sumz", action="store_true", default=False)
    strategies.add_argument("--fisher_test", action="store_true", default=False)
    strategies.add_argument("--meta_analysis", action="store_true", default=False)

    parser_create_wt.set_defaults(func=w_create_worktable)

    # ------- plot-manhattan -------

    parser_create_mp = subparsers.add_parser(
        "plot-manhattan",
        help="generate genome-wide manhattan plot for a given set of phenotypes",
    )
    parser_create_mp.add_argument(
        "--worktable-path",
        required=True,
        help="path to the worktable file containing the data",
    )
    parser_create_mp.add_argument(
        "--plot-path", required=True, help="path to the manhattan plot file to generate"
    )
    parser_create_mp.set_defaults(func=w_plot_manhattan)

    # ------- qq-plot -------

    parser_create_mp = subparsers.add_parser(
        "qq-plot",
        help="generate genome-wide qqplot of p value. Warning : require a lot of memory",
    )
    parser_create_mp.add_argument(
        "--worktable-path",
        required=True,
        help="path to the worktable file containing the data",
    )
    parser_create_mp.add_argument(
        "--plot-path", required=True, help="path to the manhattan plot file to generate"
    )
    parser_create_mp.set_defaults(func=w_qq_plot)

    # ------- plot-quadrant -------

    parser_create_mp = subparsers.add_parser(
        "plot-quadrant", help="generate a quadrant plot for a given set of phenotypes"
    )
    parser_create_mp.add_argument(
        "--worktable-path",
        required=True,
        help="path to the worktable file containing the data",
    )
    parser_create_mp.add_argument(
        "--plot-path", required=True, help="path to the quadrant plot file to generate"
    )
    parser_create_mp.add_argument(
        "--significance-treshold",
        default=5 * 10 ** -8,
        help="threshold at which a p-value is considered significant",
    )
    parser_create_mp.set_defaults(func=w_plot_quadrant)

    # ------- add-gene-annotation -------

    parser_create_mp = subparsers.add_parser(
        "add-gene-annotation",
        help="add information about genes ansd exons to the inittable",
    )
    parser_create_mp.add_argument(
        "--gene-data-path",
        required=True,
        help="path to the GFF file containing gene and exon data",
    )
    parser_create_mp.add_argument(
        "--init-table-path",
        required=True,
        help="path to the initial table file to update",
    )
    parser_create_mp.add_argument(
        "--gene-csv-path", required=False, help="path to the file df_gene.csv"
    )
    parser_create_mp.add_argument(
        "--exon-csv-path", required=False, help="path to the file df_exon.csv"
    )
    parser_create_mp.set_defaults(func=w_gene_annotation)

    # ------- extract-csv -------#
    parser_create_mp = subparsers.add_parser(
        "extract-tsv", help="Extract a table from a hdf5 repository to the tsv format. Will work for the worktables and the inittable.\nWARNING: can strongly increase storage space needed"
    )
    parser_create_mp.add_argument(
        "--hdf5-table-path",
        required=True,
        help="path to the worktable file containing the data",
    )
    parser_create_mp.add_argument(
        "--tsv-path", required=True, help="path to the tsv table"
    )
    parser_create_mp.add_argument(
        "--table-key",
        help="Existing key are 'SumStatTab' : The results of the joint analysis by SNPs - 'PhenoList' : the meta data of analysed GWAS - 'COV' : The H0 covariance used to perform joint analysis - 'GENCOV' (If present in the initTable): The genetic covariance as computed by the LDscore. Uniquely for the worktable: 'Regions' : Results of the joint analysis summarised by LD regions (Notably Lead SNPs by regions) - 'summaryTable': a double entry table summarizing the number of significant regions by test (univariate vs joint test)",
    )
    parser_create_mp.set_defaults(func=w_extract_tsv)
    
    # ------- compute predicted gain -------#
    parser_create_mp = subparsers.add_parser(
        "predict-gain", help="Predict gain based on the genetic architecture of the set of multi-trait. To function, this command need the inittable to contain genetic covariance store under the key 'GEN_COV in the inittable'"
    )
    parser_create_mp.add_argument(
        "--inittable-path",
        required=True,
        help="Path to the inittable",
    )
    parser_create_mp.add_argument(
        "--combination-path",
        required=True,
        help="Path to the file storing combination to be scored",
    )
    parser_create_mp.add_argument(
        "--gain-path", required=True, help="path to save predicted gain"
    )

    parser_create_mp.set_defaults(func=w_compute_gain)
    
    return parser


def main():
    print("", file=sys.stderr)
    print("        **     *******      *******   *******", file=sys.stderr)
    print("        **    **     **    **        **", file=sys.stderr)
    print("        **   **       **   **        **", file=sys.stderr)
    print("        **   **       **    ******    ******", file=sys.stderr)
    print("        **   ***********         **        **", file=sys.stderr)
    print(" **     **   **       **         **        **", file=sys.stderr)
    print("  *******    **       **   *******   *******", file=sys.stderr)
    print("", file=sys.stderr)
    print("", file=sys.stderr)
    parser = get_parser()
    args = parser.parse_args()
    args.func(args)


if __name__ == "__main__":
    main()
