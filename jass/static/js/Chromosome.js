class Chromosome{

	constructor(chrNumber,paper,chrsize,centroB,centroE,tabBand,mapWidth,mapHeight){

		this.chr= chrNumber;
		this.paper=paper;
		
		this.b=0;
		this.chrsize= chrsize;

		this.centroPosd=centroB;
		this.centroPosf=centroE;

		this.chrTab=tabBand;
		
		this.label='',
		
		this.mapHeight =mapHeight;
		
		
		
		this.graphicChr =null; //snap Object
		
		this.conv;
		//public var spMap:UIComponent;
		this.arcP =0; // arc parameter
	}
	prev(){}
	next(){}
	full(){}
	erase(){
		chromosome.clear();
	}
	computeMapWithCoord(d,e,isGraduate){}
	//computeMap:function(){},
	prev(){}

	isOnMapBegin(){
			 	
	
	}
	isOnMapEnd(){
	
	}
			
	redraw() {
				
	}
	 	

	createScale(isgraduate){
		
		   	//var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
		    
		   /* var svgNS = this.parentSVG.namespaceURI;


		    //var xmlns = "http://www.w3.org/2000/svg";
		    //svg.setAttribute('width',this.convert(this.mapLength));
		    //svg.setAttribute('height',this.mapHeight);

		    var inter = this.mapLength/10;
		    console.log("inter "+inter+" "+this.mapLength);
		    for (var i=0;i<10;i++){
		    	if (i/2 -Math.round(i/2) ==0){
		    		var rect = document.createElementNS(svgNS,'rect');
		    		rect.setAttribute('fill','#eceff4');
		    		rect.setAttribute('x',this.convert(i*inter));
		    		rect.setAttribute('y',this.rank*this.mapHeight);
		    		rect.setAttribute('width',this.convert(inter));
		    		rect.setAttribute('height',this.mapHeight);

		    		//console.log(parent);
		    		this.parentSVG.appendChild(rect);
		    	}

		    }*/
		  	//document.body.appendChild(svg);
	}
	convert(x)
	{
		   // console.log(this.conv);
		    var num = x *this.conv;
		    return num;
		       			
	}
	drawMapH(conv){
		this.conv =conv;
		


		var xcentrod =this.convert(this.centroPosd);
		
        var xcentrof =this.convert(this.centroPosf);

       // console.log(this.centroPosd+" "+this.centroPosf);
		//console.log(xcentrod+" "+xcentrof);

        var xcentrodArc =xcentrod -this.arcP;
        var xcentrofArc =xcentrof +this.arcP;

        var xChrf = this.convert(this.chrsize);
        //console.log("chrsize " +this.chrsize+ "xChrf "+xChrf);
        var xChrofArc = xChrf+this.arcP;

        var halfMapHeight = mapHeight/2;

        var chrbrasString = "M0,0L"+xcentrod+","+0+"S"+xcentrodArc+",20 "+xcentrod+","+mapHeight+"L0,"+mapHeight+"S-"+this.arcP+",20 0,0";
        //var chrbras2String = "M"+xcentrof+",0L"+xChrf+","+0+"S"+xChrofArc+",20 "+xChrf+","+mapHeight+"L"+xcentrof+","+mapHeight;
        
        var chrbras2String = "M"+xcentrof+",0L"+xChrf+","+0+"S"+xChrofArc+",20 "  +xChrf+","+this.mapHeight+"L"+xcentrof+","+this.mapHeight+"S"+xcentrofArc+",20 "+xcentrof+",0";
        //console.log(chrbrasString);
        //console.log(chrbras2String);
        
        
        var pathChr = paper.path(chrbrasString+chrbras2String);
        //var pathChr = paper.path(chrbrasString);
        pathChr.attr("fill", "#ffffff");
        pathChr.attr("stroke", "#000000");

        var centroString = "M"+xcentrod+",0"+"L"+xcentrof+","+this.mapHeight+"S"+xcentrofArc+",20 "+xcentrof+",0"+"L"+xcentrod+","+this.mapHeight+"S"+xcentrodArc+",20 "+xcentrod+",0";

        
        var centro = paper.path(centroString);
        centro.attr("fill","#fff5af");
        centro.attr("stroke", "#000000");
        centro.attr("title","centro");
        
        

        var chrom = paper.g(pathChr,centro);

        var chrBand;
        for (var i=0; i<this.chrTab.length; i++) {
            chrBand = this.chrTab[i];

            //console.log("ii "+chrBand['inf']+" "+this.convert(chrBand['sup'])+" "+chrBand['band']+" "+chrBand['id']);
            var band = this.createBandH(this.convert(chrBand['inf']),this.convert(chrBand['sup']),chrBand['band'],chrBand['id']);
            
            //console.log(band);
            //console.log("band "+band);
            if( band != null ) {chrom.add(band);}
            
        }
        this.graphicChr = chrom;
	}

	
	

	createBandH(b,e,band,id){

            var bArc = b-this.arcP;
            var eArc= e-this.arcP;

            var bsArc = b+this.arcP;
            var esArc= e+this.arcP;
            //console.log(id);
            var p = paper.path("M10-5-10,15M15,0,0,15M0-5-20,15").attr({
                fill: "none",
                stroke: "#000000",
                strokeWidth: 5
            });
            // To create pattern,
            // just specify dimensions in pattern method:
            p = p.pattern(0, 0, 10, 10);



            var colBand = { "gpos25" :  "#999999", "gpos50" :     "#555555", "gpos75":  "#333333" ,"gpos100": "#111111","stalk":p,"gvar":"#ffffff"};  

            if((id != "gneg")&&(id != "acen")&&(id != "gvar")){
                var bandString;
                if (b<this.convert(this.centroPosf)){
                	bandString="M"+b+",0"+"L"+e+",0"+"S"+eArc+",20 "+e+","+mapHeight+"L"+b+","+mapHeight+"S"+bArc+",20 "+b+",0Z" ;  
                }
                else{
                	bandString="M"+b+",0"+"L"+e+",0"+"S"+esArc+",20 "+e+","+mapHeight+"L"+b+","+mapHeight+"S"+bsArc+",20 "+b+",0Z" ;  
                }
                //var bandString="M"+b+",0"+"L"+e+",0"+"S"+eArc+",20 "+e+","+mapHeight+"L"+b+","+mapHeight+"S"+bArc+",20 "+b+",0Z" ;  

                var bandP = paper.path(bandString);
                
                //bandP.attr("fill", "#93aac5");
                if (colBand[id] == undefined){
                    console.log(id);
                }
                bandP.attr("fill", colBand[id]);

                bandP.attr("title", band);
                //var title =;
                bandP.append(Snap.parse(band));
                //bandP.translate(50,50);

                //bandP.transform(t);
                return bandP
            }
            else return null;
        }

    createBand(b,e,band,id){

		/*paper.path(Snap.format("M{x},{y}h{dim.width}v{dim.height}h{dim['negative width']}z", {
		    x: 10,
		    y: 20,
		    dim: {
		        width: 40,
		        height: 50,
		        "negative width": -40
		    }
		}));*/

		console.log("Chromosomes createBandH ");
		var bandP =null;
		var bArc = b-this.arcP;
        var eArc= e-this.arcP;
        //console.log("createBandH"+b+" "+e+" "+band+" "+id);

        // pattern pour bande chromosomique snag
        var p = paper.path("M10-5-10,15M15,0,0,15M0-5-20,15").attr({
            fill: "#555555",
            stroke: "#00FF00",
            strokeWidth: 5
        });
        // To create pattern,
        // just specify dimensions in pattern method:
        p = p.pattern(0, 0, 10, 10);

        console.log("mapHeight "+mapHeight+" "+this.mapHeight);

        var colBand = { "gpos25" :  "#999999", "gpos50" :     "#555555", "gpos75":  "#333333" ,"gpos100": "#111111","stalk":p,"gvar":"#ff00ff"};  
        console.log("id " +id);
        if((id != "gneg")&&(id != "acen")&&(id != "gvar")){
            var bandString="M"+b+",0"+"L"+e+",0"+"S"+eArc+",20 "+e+","+this.mapHeight+"L"+b+","+this.mapHeight+"S"+bArc+",20 "+b+",0Z" ;  

            var bandP = paper.path(bandString);
            
            //bandP.attr("fill", "#93aac5");
            if (colBand[id] == undefined){
                console.log(id);
            }

            bandP.attr("fill", colBand[id]);

            bandP.attr("title", band);
            //var title =;
            //bandP.append(Snap.parse(band));
            //bandP.translate(50,50);

            //bandP.transform(t);
            //return bandP;
        }
        //else return null;
        return bandP;
	}

	addZoom(pos){
	    
            console.log("addZoom4 New");
            var zoomSize = 1000000;
            var xzoomSize = this.convert(zoomSize);

            var zoomHeight = mapHeight +5;

            var zoomString = "M0,-5L"+xzoomSize+",-5L"+xzoomSize+","+zoomHeight+"L0,"+zoomHeight+"Z";
            var zoom =  paper.path(zoomString);
            zoom.attr("fill","#00ffff");
            zoom.attr("stroke", "#000000");
            zoom.attr("alpha", 0.5);
            //zoom.transform(t);

            zoom.mouseover(this.f_in);
            zoom.attr("title","zoomBar");
            this.graphicChr.add(zoom);
		
	    //paper.text(this.convert(pos),0, ['The Three Layers','of','Every Web Page']+pos).attr({fill: '#FBAF3F', fontFamily: 'Impact'});

            // move zoom to Position in chr
            var t = new Snap.Matrix();
            //t.translate(0, this.convert(pos));

            t.translate(this.convert(pos),0);
            zoom.animate({ transform: t }, 1000, mina.bounce );
            
	    
            $('#chrZoom p').text('Zoom 0 '+xzoomSize ); 
            zoom.drag(this.move, this.start, this.stop );


        }
    f_in(){
    	console.log('test');
    }
    move(dx,dy,x,y) {
    	//(onmove, onstart, onend, move_scope, start_scope, end_scope)
            //if((dx>0) && (dx <xChrf)){
           	console.log("!evt "+x+" "+y+" "+dx+" "+dy);
            if(x>=60){
            	$('#chrZoom p').text('Zoom '+dx+' '+dy ); 
                this.attr({

                    transform: this.data('origTransform') + (this.data('origTransform') ? "T" : "t") + [dx, 0]
                });
            }
            else{
            	stop();
            }
    	console.log('move '+ dx);
    }

    start() {
    	console.log('start');
        this.data('origTransform', this.transform().local );
    }
    stop() {
        console.log('finished dragging');
    }
	drawMapV(){

	}
}
