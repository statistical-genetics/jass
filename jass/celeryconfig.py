import os

## Broker settings.
# broker_url = os.getenv("JASS_RABBITMQ_URL", "amqp://guest:guest@localhost:5672//")
from jass.config import config

broker_url = "amqp://%(user)s:%(pwd)s@%(host)s:%(port)s//" % dict(
    user=os.getenv('RABBITMQ_USER', 'guest'),
    pwd=os.getenv('RABBITMQ_PASSWORD', 'guest'),
    host=os.getenv('RABBITMQ_HOST', 'localhost'),
    port=os.getenv('RABBITMQ_PORT', '5672'),
)

## Broker settings.
# result_backend = os.getenv('JASS_RABBITMQ_URL','amqp://guest2:guest@localhost:5672//')
# result_backend = "rpc://"
result_backend = f'file://{config["CELERY_RESULTS_BACKEND_DIR"]}'  # if rpc become not a solution
# List of modules to import when the Celery worker starts.
# imports = ('myapp.tasks',)

## Using the database to store task state and results.
# result_backend = 'db+sqlite:///results.db'

# task_annotations = {'tasks.add': {'rate_limit': '10/s'}}
# task_track_started = True
task_acks_late = True  # needed as worker can be killed by k8s when consuming to much RAM
