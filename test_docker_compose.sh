#!/usr/bin/env bash

if [ "$(docker compose ps | grep jass | grep -v Exit | wc -l)" == "0" ]; then
  docker compose -f docker-compose.yaml -f docker-compose.test.yaml up -d --build || exit 99
  echo "Waiting some time for interface to be up"
  sleep 5
else
  rm -rf projects/project_407db5eb73e8b3a0dd9fd8a3c018db6d
fi


echo "Testing /"
curl -I http://0.0.0.0:3001/ \
  1> log/01-index.log \
  2> log/01-index.err

if [ "$(cat log/01-index.log | grep "200 OK" | wc -l)" == "0" ]; then exit 01; fi


echo "Testing /phenotypes"
curl -I http://0.0.0.0:3001/phenotypes/ \
  1> log/02-phenotypes.log \
  2> log/02-phenotypes.err

if [ "$(cat log/02-*.log | grep "200 OK" | wc -l)" == "0" ]; then exit 02; fi


echo "Test fetching phenotypes through API"
curl --location --request POST http://0.0.0.0:8080/api/phenotypes/ \
--header 'Content-Type: application/json' \
--data-raw '{"initTableName":"initTableTest1.hdf5"}' \
  -o log/11-post-phenotypes.json \
  1> log/11-post-phenotypes.log \
  2> log/11-post-phenotypes.err

if [ "$(cat log/11-*.json | grep "Internal Server Error" | wc -l)" == "1" ]; then exit 11; fi


echo "Test fetching phenotypes through API"
curl --location --request POST http://0.0.0.0:8080/api/phenotypes/ \
--header 'Content-Type: application/json' \
--data-raw '{"initTableName":"initTable.hdf5"}' \
  -o log/12-post-phenotypes.json \
  1> log/12-post-phenotypes.log \
  2> log/12-post-phenotypes.err

if [ "$(cat log/12-*.json | grep "Internal Server Error" | wc -l)" == "1" ]; then exit 12; fi

if [ "$(cat log/11-*.json)" == "$(cat log/12-*.json)" ]; then echo "should not be equal"; exit 12; fi


echo "Testing project creation"
curl --location --request POST 'http://0.0.0.0:8080/api/projects' \
--header 'Accept: text/html' \
--header 'Content-Type: application/json' \
--data-raw '{"phenotypeID": ["z_MAGIC_FAST-GLUCOSE", "z_MAGIC_FAST-INSULIN"]}' \
  -o log/03-post-project.json \
  1> log/03-post-project.log \
  2> log/03-post-project.err

PROJECT_ID=$(jq .id log/03-post-project.json | sed 's/\"//g')
if [ ! -d "projects/project_$PROJECT_ID" ]; then
  exit 03
fi


echo "Trigger running"
curl --location http://0.0.0.0:8080/api/projects/$PROJECT_ID/ \
  -o log/04-project.json \
  1> log/04-project.log \
  2> log/04-project.err

if [ ! -f projects/project_$PROJECT_ID/meta.json ]; then exit 04; fi


echo "Waiting some time project to be completed"

while [ $(grep CREATING projects/project_$PROJECT_ID/meta.json | wc -l) == "1" ]; do
  echo "... still running"
  sleep 1
done
echo "... done"

if [ $(grep ERROR projects/project_$PROJECT_ID/meta.json | wc -l) == "1" ]; then exit 14; fi

echo "Testing manhattan plot"
curl --location http://0.0.0.0:8080/api/projects/$PROJECT_ID/globalmanhattan \
  -o log/05-globalmanhattan.png \
  1> log/05-globalmanhattan.log \
  2> log/05-globalmanhattan.err

if [ ! -s log/05-*.png ]; then exit 05; fi


echo "Testing quadrant plot"
curl --location http://0.0.0.0:8080/api/projects/$PROJECT_ID/quadrant \
  -o log/06-quadrant.png \
  1> log/06-quadrant.log \
  2> log/06-quadrant.err

if [ ! -s log/06-*.png ]; then exit 06; fi


echo "Testing genome_full"
curl --location http://0.0.0.0:8080/api/projects/$PROJECT_ID/genome_full \
  -o log/07-genome_full.csv \
  1> log/07-genome_full.log \
  2> log/07-genome_full.err

if [ ! -s log/07-*.csv ]; then exit 07; fi

echo "Waiting for worktable"
I=0
while  [[ "$(jq .status.worktable projects/project_$PROJECT_ID/meta.json)" != "\"READY\"" ]]; do
  I=$(expr $I + 1)
  if [ $I -gt 120 ]; then
    echo "Waited too long, exiting"
    exit 77
  fi
  echo "... waiting for worktable"
  sleep 1
done
echo "... done"

echo "Testing heatmap"
curl --location http://0.0.0.0:8080/api/projects/$PROJECT_ID/heatmap/chr9/Region699 \
  -o log/08-heatmap.csv \
  1> log/08-heatmap.log \
  2> log/08-heatmap.err

if [ "$(cat log/08-*.csv | wc -l)" -le "2" ]; then exit 08; fi


echo "Testing manhattan"
curl --location http://0.0.0.0:8080/api/projects/$PROJECT_ID/manhattan/chr9/Region699 \
  -o log/09-manhattan.csv \
  1> log/09-manhattan.log \
  2> log/09-manhattan.err

if [ "$(cat log/09-*.csv | wc -l)" -le "2" ]; then exit 09; fi

echo "SUCCESS !!!"

exit 0