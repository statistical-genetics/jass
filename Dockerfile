FROM python:3.10-bullseye as backend

ENV JASS_DATA_DIR '/data'
ENV JASS_PROJECTS_DIR '/projects'

EXPOSE 8080

CMD ["uvicorn", "jass.server:app", "--host", "0.0.0.0", "--port", "8080"]

RUN addgroup --gid 1000 kiwi \
 && adduser --disabled-password --gecos '' --uid 1000 --gid 1000 kiwi \
 && apt-get update \
 && apt-get install -y \
        nano \
        wget \
        rsync \
 && rm -rf /var/lib/apt/lists/* \
 && pip install --upgrade pip setuptools \
 && mkdir /code \
 && mkdir ${JASS_DATA_DIR} \
 && chown kiwi:kiwi ${JASS_DATA_DIR} \
 && mkdir ${JASS_PROJECTS_DIR} \
 && chown kiwi:kiwi ${JASS_PROJECTS_DIR}
WORKDIR /code

COPY requirements*.txt /code/
RUN pip install -r requirements.txt \
 && pip3 install -U watchdog[watchmedo]

COPY ./*-entrypoint.sh /
RUN chmod a+x /*-entrypoint.sh
ENTRYPOINT ["/docker-entrypoint.sh"]

COPY . /code/
RUN pip install -e .

USER kiwi
