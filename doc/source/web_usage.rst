Web usage
=========

The web interface of JASS allows you to launch joint analyses over different datasets, and explore their results.

.. image:: _static/jass_welcome_page.png
  :width: 100%
  :alt: JASS welcome page

The next screen lets you choose the datasets that you wish to analyse

.. image:: _static/jass_select_phenotypes.png
  :width: 100%
  :alt: JASS phenotype selection page
