Compute JASS power gain from the genetic architecture of traits
===============================================================

In a recent study :cite:`suzuki2023trait`, we explore how the genetic architecture of the set of traits (heritability, genetic covariance, heritability undetected by the univariate test, ...) can be predictive of statistical power gain of the multi-trait test.



We implement an additional command line tool to give access our predictive model (the **jass predict-gain** command). 
This command allows the to score swiftly a large number of traits combinations and to focus on set of traits the most promising for multi-trait testing.

To work the inittable provided to the **jass predict-gain** command must contain the genetic covariance between traits. 


.. code-block:: shell

    jass predict-gain --inittable-path inittable_curated_111_traits_20-03-2024.hdf5 --combination_path ./combination_example.tsv --gain-path predicted_gain.tsv


The second argument (--combination_path) is a path to a file containing the set of traits to be scored.

.. csv-table:: Set of traits
  :widths: 20, 70
  :header-rows: 1

    GRP1,z_GIANT_HIP z_GLG_HDL z_GLG_LDL z_MAGIC_2HGLU-ADJBMI
    GRP2,z_SPIRO-UKB_FVC z_SPIRO-UKB_FEV1 z_TAGC_ASTHMA

When executed the command will created a report at --gain-path

.. csv-table:: Predicted gain
    :header-rows: 1

    traits,k,avg_distance_cor,mean_gencov,avg_Neff,avg_h2,avg_perc_h2_diff_region,log10_mean_gencov,log10_avg_distance_cor,gain
    ['z_SPIRO-UKB_FVC'; 'z_SPIRO-UKB_FEV1'; 'z_TAGC_ASTHMA'],0.1,0.1731946683845993,0.0637,0.3843393026739591,0.2785193310634847,0.7976315890930669,0.8139196701681637,0.8013809378674498,0.06428524764535551
    ['z_GIANT_HIP'; 'z_GLG_HDL'; 'z_GLG_LDL'; 'z_MAGIC_2HGLU-ADJBMI'],0.2,0.14899001074867035,0.01535,0.12076877719858631,0.22628198390356655,0.9055326131023057,0.6573854616675169,0.7879956172999502,-0.010766494024690904

The last column provide the predicted gain ("the higher the more promising"). Note that extrapoling on new data might give lesser performances than reported in :cite:`suzuki2023trait`.

.. bibliography:: reference.bib