Command line reference
----------------------

This section contains the exhaustive list of the sub-commands and options available for JASS on the command line.

.. argparse::
   :ref: jass.__main__.get_parser
   :prog: jass

Utilities
~~~~~~~~~~~~~~~~~~~

.. argparse::
   :ref: scripts.hdf5_add_attributes.get_parser
   :prog: scripts/hdf5_add_attributes
