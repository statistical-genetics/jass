
What is JASS?
=============

JASS is a python package that handles the computation of joint statistics over sets of selected GWAS results, and the interactive exploration of the results through a web interface or static graph (https://jass.pasteur.fr/index.html). JASS is a highly versatile tool that can be used either as :

* (i) an online web service, or

* (ii) a command line tool that one can use independently for development purposes.

More precisely, The generation of joint statistics over a set of selected studies, and the generation of static plots to display the results, is easily performed using the command line interface. These functionalities can also be accessed through a web application embedded in the python package, which also enables the exploration of the results through a dynamic JavaScript interface.The JASS analysis module handles the data processing, going from the import of the data up to the computation of the joint statistics and the generation of the various static plots to illustrate the results.

In this documentation, we cover first the steps required for installing the software, and illustrate its usage through a reproducible example to guide the users.

We also briefly describe in the next section the pre-processing of raw GWAS data which can be performed through a companion script provided on behalf of the JASS package.

For method details and application inspiration check out our publications with JASS or its accompanying packages (RAISS):

JASS application paper :cite:`julienne2021multitrait`

JASS computational architecture :cite:`julienne2020jass`

RAISS :cite:`10.1093/bioinformatics/btz466`

.. bibliography:: reference.bib
