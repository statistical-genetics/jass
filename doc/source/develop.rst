Developer documentation
=======================

The JASS source code is hosted at the `Gitlab instance of the Institut Pasteur <https://gitlab.pasteur.fr/statistical-genetics/jass>`_.

API
---

.. automodule:: jass
   :members:

.. autosummary::
   :toctree: _autosummary
