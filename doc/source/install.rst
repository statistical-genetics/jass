Installation
============

You can use JASS locally either using the command line interface in a terminal, or by running a web server. Deployment in a public server is also later discussed in this document.

You need **python3** to install and use JASS. As of April 2022, JASS runs on python from 3.8 to 3.10.

Installation with pip (recommended)
-----------------------------------
We advise users to install jass in a virtual environment

.. code-block:: shell

	python3 -m venv $PATH_NEW_ENVIRONMENT
	source $PATH_NEW_ENVIRONMENT/bin/activate
	pip3 install git+https://gitlab.pasteur.fr/statistical-genetics/jass.git
	pip3 install -r https://gitlab.pasteur.fr/statistical-genetics/jass/-/raw/master/requirements.txt


Installation with conda
-----------------------

A procedure for the correct installation of JASS in Anaconda environment for development purpose is developed in detail below.

**1. Create a specific directory and load the code on your laptop (only the first time)**

For example, the directory $HOME/DEVELOP_JASS has been created.
In a TERMINAL window, type the following instructions:

.. code-block:: shell

	cd $HOME/DEVELOP_JASS
	git clone https://gitlab.pasteur.fr/statistical-genetics/jass

**2. Change directory to the JASS main directory**

.. code-block:: shell

	cd $HOME/DEVELOP_JASS/jass

**3. Create a specific virtual ANACONDA environment for JASS:**

.. note::

	You have to check that you are in the **base** environment of Anaconda before creating the new virtual environment:

.. code-block:: shell

	conda create --name Dev_Jass_Python python=3.10

**4. Activation of the new environment**

.. code-block:: shell

	conda activate Dev_Jass_Python

**5. Installing JASS dependencies**

.. code-block:: shell

	pip install -r requirements.txt

**6. Installing JASS for development purpose**

.. code-block:: shell

	pip install -e .

**7. Installation verification**

.. code-block:: shell

	pip freeze

Installing it this way will automatically import and setup all of the dependencies required to run JASS.

This is pretty much all you need to do to use JASS on the command line, or to run a local personal web server. To deploy JASS on a public web server, please refer to the "Public server deployment" section.

Additional software installation on Linux
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Some python packages require additional non-python software that you might need to install, e.g. on Ubuntu, with:

.. code-block:: shell

    sudo apt install libfreetype6-dev #(required by matplotlib)
    sudo apt install libhdf5-dev #(required by tables)
    sudo apt install rabbitmq-server #(required by celery)

Additional software installation on Windows
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. tip:: For Windows, an easy option is to install the free software package manager called chocolatey.

In order to have a correct installation of RabbitMQ on windows, we recommend to install chocolatey by running the following command as Administrator in a powershell:

.. code-block:: shell

	Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

Then, you can install RabbitMQ with the "choco install" command:

.. code-block:: shell

	Choco install rabbitmq

Run JASS as a web application (optional)
----------------------------------------

To run locally JASS as a web application, it is recommended to use docker compose as it handles all dependencies in one command.
If using docker compose is not an option, you need to launch two servers in two different processes, the `celery` task management server and the web server. The web server handles the HTTP requests, and sends all computation requests to the task management server.

Launching with docker compose (recommended)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Once docker is installed, copy the :ref:`initTable.hdf5 you created <Create initTable>` in data/ folder and then run

.. code-block:: shell

    # build the application
    docker compose build
    # launch the whole application
    docker compose up
    # then visit http://0.0.0.0:3000/

If you just want to try jass with test data (i.e without creating an initTable), run:

.. code-block:: shell

    # build the application
    docker compose build
    # launch the whole application with
    docker compose -f docker-compose.yaml -f docker-compose.test.yaml up
    # then visit http://0.0.0.0:3000/

launching the two servers on Linux
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The command lines below show how to launch the two servers. Please note that you should of course not use this for any use beyond tests and personnal use, we provide further instructions below to deploy JASS on shared/public servers.

.. code-block:: shell

    # launch celery to process tasks
    celery -A jass worker
    ## and in ANOTHER TERMINAL
    # launch the web server
    jass serve

By default, the Jass server will listen to requests on the port 8080 of your machine. You can control the host and port that the JASS standalone webserver listens to through two environment variables, ``JASS_HOST`` and ``JASS_PORT``, that you just have to set before to launch the web server.

Launching the celery server on Windows
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In order to launch the celery server on windows, it is necessary to use the following command in a terminal:

.. code-block:: shell

	celery -A jass worker --pool=solo

.. warning::
	The command recommended for Linux crashes when it is used on windows due to incorrect recognition of the prefork option on windows by the billiard library.
	The part “--pool=solo” is necessary on windows because this is the only option of celery that works on windows.

Launching the web server on Windows
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The web server is launched  in another terminal (the same as on Linux):

.. code-block:: shell

	jass serve

Public server deployment (optional)
-----------------------------------

Helm charts are available in the `chart folder of the source repository <https://gitlab.pasteur.fr/statistical-genetics/jass/tree/master/chart>`_. These charts automate the installation and update of the application in a kubernetes cluster.

In this specific deployment, the JASS web application is hosted by an NGINX server, served by the uvicorn library. It communicates with a celery container that handles the user-launched tasks. Many other deployment options are of course possible, use whichever suits your infrastructure!
