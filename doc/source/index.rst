.. JASS documentation master file, created by
   sphinx-quickstart on Thu Mar 15 21:08:10 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

JASS documentation
==================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   about
   install
   data_import
   generating_joint_analysis
   get_predicted_gain
   command_line_usage
   web_usage
   web_admin
   develop

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
