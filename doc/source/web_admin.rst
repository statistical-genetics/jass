Web admin
===============================================================================

Updating data
-------------------------------------------------------------------------------

You can update data using kubectl, to do so see this `"How To" <https://gitlab.pasteur.fr/statistical-genetics/jass/-/blob/master/chart/HOWTO.md?ref_type=heads#updating-data-including-inittablehdf5>`_.

An alternative using fex.pasteur.fr is also proposed, with no binary to install locally.

Upload to fex
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Go to https://fex.pasteur.fr/fup, click on "fex yourself" and choose the file you want to send.
In our example it is initTableTest.hdf5

.. image:: _static/fex-upload.png
  :width: 100%

Get the fex link
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You then receive an email containing the url where the initTable is available, copy it.
In our example it is https://dl.pasteur.fr/fop/6atgurR3/initTableTest.hdf5

.. image:: _static/fex-email.png
  :width: 100%

Go to gitlab pipelines
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Go to https://gitlab.pasteur.fr/statistical-genetics/jass/-/pipelines

Click on the last grey/green circle on the right (indicated 1 in the following image).
Then on the task ``upload-from-fex-to-dev`` (indicated 2) if you want to send the file in dev.
A job is also proposed related to master pipelines.

.. image:: _static/gitlab-pipelines.png
  :width: 100%

Trigger while providing the URL
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Provide in an variable name ``FEX_URL`` the url.
In our example ``FEX_URL=https://dl.pasteur.fr/fop/6atgurR3/initTableTest.hdf5``

.. image:: _static/trigger-job-with-url.png
  :width: 100%

After some time the job is completed, you see with the ls that the file is now there.

After the job is completed, in the right part you see "Job artifact", when browsing them you have the
file you have added, and the previous one that was replaced.
Artefact are keep 1 month as of March 2024.

.. image:: _static/job-done-with-url.png
  :width: 100%

Deleting data
-------------------------------------------------------------------------------

You can also simply delete hdf5 file (and only them).

Go to https://gitlab.pasteur.fr/statistical-genetics/jass/-/pipelines

Click on the last grey/green circle on the right (indicated 1 in the following image).
Then on the task ``delete-data-from-dev`` (indicated 2).

.. image:: _static/gitlab-pipelines-delete-file.png
  :width: 100%

Trigger while providing the filename
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Provide in an variable name ``HDF5_FILENAME`` the hdf5 filename (no relative path allowed).
In our example ``HDF5_FILENAME=initTableTest.hdf5``

.. image:: _static/trigger-job-with-filename.png
  :width: 100%

Also in the right part you see "Job artifact", when browsing them you have the
file you just removed.
Artefact are keep 1 month as of March 2024.

.. image:: _static/job-done-with-filename.png
  :width: 100%