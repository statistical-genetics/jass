#!/bin/bash

function msg_info {
   echo -e "\033[1;32m$1\033[0m"
}

function msg_warning {
   echo -e "\033[1;33m$1\033[0m"
}

function msg_error {
   echo -e "\033[1;31m$1\033[0m"
}

cd /code

if [ "$1" == "hold_on" ]; then
    msg_info "holding on util you delete /tmp/hold_on"
    touch /tmp/hold_on
    while [ -e "/tmp/hold_on" ]; do
        sleep 1 ;
        echo "holding on" ;
    done
fi

#if [ "${WAIT_FOR_DATA_DIR_READY}" == "1" ]; then
#    while [ ! -f ${DATA_DIR_READY} ]; do
#        msg_info "Waiting for data dir ot flagged ready with ${DATA_DIR_READY}"
#        sleep 5
#    done
#else
#    msg_info "Copying file from /code/data to ${JASS_DATA_DIR}"
#    rsync -a /code/data/ ${JASS_DATA_DIR}/
#    export INIT_TABLE_PATH="${JASS_DATA_DIR}/initTable.hdf5"
#    if [ ! -f ${INIT_TABLE_PATH} ]; then
#        msg_info "$INIT_TABLE_PATH missing, downloading it from $INIT_TABLE_URL"
#        wget "${INIT_TABLE_URL}" --output-document=${INIT_TABLE_PATH}
#    else
#        msg_info "$INIT_TABLE_PATH found"
#    fi
#    touch ${DATA_DIR_READY}
#fi


exec "$@"
