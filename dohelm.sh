#!/usr/bin/env bash

# for the first run, do an `helm dependency update chart`
# To delete the app : `helm delete $(git branch --show)`
# To delete PERMANENTLY all pvc : `kubectl delete pvc --all`

# source ./tokens.sh # put `export SECRET_KEY="..."` in this file

CI_REGISTRY="registry-gitlab.pasteur.fr"
NAMESPACE="jass-dev"
CI_REGISTRY_IMAGE="${CI_REGISTRY}/statistical-genetics/jass"
CI_COMMIT_SHA=$(git log --format="%H" -n 1)
#CI_COMMIT_SHA="8e1c38c0b93cbf5144a30d990a1bea29c0fcb0f0"
CI_COMMIT_REF_SLUG=$(git branch --show)
PUBLIC_URL="jass-${CI_COMMIT_REF_SLUG}.dev.pasteur.cloud"
PUBLIC_URL_2="jass.pasteur.fr"

export ACTION="upgrade --install"
#export ACTION="template --debug"

RABBITMQ_PASSWORD=$(kubectl get secret --namespace "jass-dev" ${CI_COMMIT_REF_SLUG}-rabbitmq -o jsonpath="{.data.rabbitmq-password}" | base64 --decode)
RABBITMQ_ERLANG_COOKIE=$(kubectl get secret --namespace "jass-dev" ${CI_COMMIT_REF_SLUG}-rabbitmq -o jsonpath="{.data.rabbitmq-erlang-cookie}" | base64 --decode)


helm ${ACTION} --namespace=${NAMESPACE} \
    --set CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE} \
    --set image.tag=${CI_COMMIT_SHA} \
    --set ingress.host.name=${PUBLIC_URL} \
    --set registry.username=${DEPLOY_USER} \
    --set registry.password=${DEPLOY_TOKEN} \
    --set registry.host=${CI_REGISTRY} \
    --set imagePullSecrets[0].name="registry-pull-secret-${CI_COMMIT_REF_SLUG}" \
    --set rabbitmq.auth.password=$RABBITMQ_PASSWORD \
    --set rabbitmq.auth.erlangCookie=$RABBITMQ_ERLANG_COOKIE \
    --values ./chart/values.${NAMESPACE}.yaml \
    ${CI_COMMIT_REF_SLUG} ./chart/